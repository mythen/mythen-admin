import * as React from 'react';
import { useSelector } from 'react-redux';
import { Divider, useMediaQuery } from '@material-ui/core';
import { MenuItemLink, useTranslate } from 'react-admin';
import { withRouter } from 'react-router-dom';
import LabelIcon from '@material-ui/icons/Label';

const Menu = ({ onMenuClick, logout }) => {
  const isXSmall = useMediaQuery(theme => theme.breakpoints.down('xs'));
  const open = useSelector(state => state.admin.ui.sidebarOpen);
  const translate = useTranslate();
  return (
    <div>
      <Divider />
      <MenuItemLink
        to="/myths"
        primaryText={translate(`resources.myths.name`)}
        leftIcon={<LabelIcon />}
        onClick={onMenuClick}
        sidebarIsOpen={open}
      />
      <MenuItemLink
        to="/sources"
        primaryText={translate(`resources.sources.name`)}
        leftIcon={<LabelIcon />}
        onClick={onMenuClick}
        sidebarIsOpen={open}
      />
      <MenuItemLink
        to="/hylem_sequences"
        primaryText={translate(`resources.hylem_sequences.name`)}
        leftIcon={<LabelIcon />}
        onClick={onMenuClick}
        sidebarIsOpen={open}
      />
      <MenuItemLink
        to="/hylems"
        primaryText={translate(`resources.hylems.name`)}
        leftIcon={<LabelIcon />}
        onClick={onMenuClick}
        sidebarIsOpen={open}
      />
      <Divider />
      <MenuItemLink
        to="/subjects"
        primaryText={translate(`resources.subjects.name`)}
        leftIcon={<LabelIcon />}
        onClick={onMenuClick}
        sidebarIsOpen={open}
      />
      <MenuItemLink
        to="/predicates"
        primaryText={translate(`resources.predicates.name`)}
        leftIcon={<LabelIcon />}
        onClick={onMenuClick}
        sidebarIsOpen={open}
      />
      <MenuItemLink
        to="/hylem_objects"
        primaryText={translate(`resources.hylem_objects.name`)}
        leftIcon={<LabelIcon />}
        onClick={onMenuClick}
        sidebarIsOpen={open}
      />
      <MenuItemLink
        to="/types"
        primaryText={translate(`resources.types.name`)}
        leftIcon={<LabelIcon />}
        onClick={onMenuClick}
        sidebarIsOpen={open}
      />
      <MenuItemLink
        to="/source_localities"
        primaryText={translate(`resources.source_localities.name`)}
        leftIcon={<LabelIcon />}
        onClick={onMenuClick}
        sidebarIsOpen={open}
      />
      <MenuItemLink
        to="/source_objects"
        primaryText={translate(`resources.source_objects.name`)}
        leftIcon={<LabelIcon />}
        onClick={onMenuClick}
        sidebarIsOpen={open}
      />
      <Divider />
      {isXSmall && logout}
    </div>
  );
}

export default withRouter(Menu);
