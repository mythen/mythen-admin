import { Layout } from 'react-admin';
import Menu from './Menu';
import React from 'react';

const CustomLayout = (props) => {
  return (
    <Layout
      {...props}
      menu={Menu}
    />
  );
};

export default CustomLayout;