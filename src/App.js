import authProvider from './providers/authProvider';
import { entrypoint, dataProvider } from './providers/dataProvider';
import React from 'react';
import { Resource } from 'react-admin';

import { HydraAdmin, ResourceGuesser } from '@api-platform/admin';
import { SubjectCreate, SubjectEdit, SubjectList, SubjectShow } from './components/subject/Subject';
import {
  PredicateCreate,
  PredicateEdit,
  PredicateList,
  PredicateShow
} from './components/predicate/Predicate';
import {
  HylemObjectCreate,
  HylemObjectEdit,
  HylemObjectList,
  HylemObjectShow
} from './components/hylem-object/HylemObject';
import { SourceCreate, SourceEdit, SourceShow, SourceList } from './components/Source';
import { MythCreate, MythEdit, MythShow, MythList } from './components/Myth';
import { TypeCreate, TypeEdit, TypeShow, TypeList } from './components/Type';
import {
  HylemSequenceCreate,
  HylemSequenceEdit,
  HylemSequenceList,
  HylemSequenceShow
} from './components/hylem-sequence/index';
import {
  HylemList,
  HylemShow
} from './components/hylem-sequence/Hylem';
import { PersonCreate, PersonEdit, PersonShow, PersonList } from './components/Person';
import { LocalityCreate, LocalityEdit, LocalityShow, LocalityList } from './components/Locality';
import { SourceObjectCreate, SourceObjectEdit, SourceObjectShow, SourceObjectList } from './components/SourceObject';
import { SWRConfig } from 'swr';
import { fetcherSWR } from './server/api';
import i18nProvider from './i18n/i18nProvider';
import { isAdmin } from './services/token.service';
import { Divider } from '@material-ui/core';
import CustomLayout from './Layout';

export default () => (
  <SWRConfig value={{ fetcher: fetcherSWR }}>
    <HydraAdmin entrypoint={entrypoint} dataProvider={dataProvider} authProvider={authProvider} i18nProvider={i18nProvider} layout={CustomLayout} disableTelemetry={true}>
      <ResourceGuesser name="myths"
        create={isAdmin() ? MythCreate : null}
        edit={isAdmin() ? MythEdit : null}
        show={MythShow}
        list={MythList}
      />
      <ResourceGuesser
        name="sources"
        create={isAdmin() ? SourceCreate : null}
        edit={isAdmin() ? SourceEdit : null}
        show={SourceShow}
        list={SourceList}
      />
      <ResourceGuesser
        name="hylem_sequences"
        create={isAdmin() ? HylemSequenceCreate : null}
        edit={isAdmin() ? HylemSequenceEdit : null}
        list={HylemSequenceList}
        show={HylemSequenceShow}
      />
      <ResourceGuesser
        name="hylems"
        create={null}
        edit={null}
        list={HylemList}
        show={HylemShow}
      />
      <Divider />
      <ResourceGuesser
        name="subjects"
        create={isAdmin() ? SubjectCreate : null}
        edit={isAdmin() ? SubjectEdit : null}
        list={SubjectList}
        show={SubjectShow}
      />
      <Resource name="subject_determinations" />
      <Resource name="predicate_determinations" />
      <Resource name="hylem_object_determinations" />
      <Resource name="hylem_sequence_items" />
      <Resource name="hylems" />
      <Resource name="gaps" />
      <Resource name="headlines" />
      <ResourceGuesser
        name="predicates"
        create={isAdmin() ? PredicateCreate : null}
        edit={isAdmin() ? PredicateEdit : null}
        list={PredicateList}
        show={PredicateShow}
      />
      <ResourceGuesser
        name="hylem_objects"
        create={isAdmin() ? HylemObjectCreate : null}
        edit={isAdmin() ? HylemObjectEdit : null}
        list={HylemObjectList}
        show={HylemObjectShow}
      />
      <ResourceGuesser name="types"
        create={isAdmin() ? TypeCreate : null}
        edit={isAdmin() ? TypeEdit : null}
        show={TypeShow}
        list={TypeList}
      />
      <ResourceGuesser name="people"
        create={isAdmin() ? PersonCreate : null}
        edit={isAdmin() ? PersonEdit : null}
        show={PersonShow}
        list={PersonList}
      />
      <ResourceGuesser
        name="source_localities"
        create={isAdmin() ? LocalityCreate : null}
        edit={isAdmin() ? LocalityEdit : null}
        show={LocalityShow}
        list={LocalityList}
      />
      <ResourceGuesser
        name="source_objects"
        create={isAdmin() ? SourceObjectCreate : null}
        edit={isAdmin() ? SourceObjectEdit : null}
        show={SourceObjectShow}
        list={SourceObjectList}
      />
    </HydraAdmin>
  </SWRConfig>
);
