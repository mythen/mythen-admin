import api, { removeApiTxt } from "../server/api";

// Add headline
export async function addHeadline(hylemTime, headline, sortValue, onError) {
  try {
    // Add HEADLINE
    const { data: headlineData } = await api.post("/headlines", headline);
    // Add Hylem sequence items
    await api.post("/hylem_sequence_items", {
      headline: headlineData['@id'],
      itemsTime: hylemTime,
      itemType: "Headline",
      chronSort: sortValue,
      storySort: sortValue
    })

    alert('Created Headline successfully');

    window.location.reload();
  } catch {
    onError('Failed to add Headline');
  };
};

// Edit headline
export async function editHeadline(headlineId, headline, onError) {
  try {
    // Edit HEADLINE
    await api.put(removeApiTxt(headlineId), headline);

    alert('Edited headline successfully');

    window.location.reload();
  } catch {
    onError('Failed to edit headline');
  }
}
