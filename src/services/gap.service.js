import api, { removeApiTxt } from '../server/api';

// Add GAP
export async function addGap(hylemTime, gap, sortValue, onError) {
  try {
    // Add GAP
    const { data: gapData } = await api.post("/gaps", gap);
    // Add Hylem sequence items
    await api.post("/hylem_sequence_items", {
      gap: gapData['@id'],
      itemsTime: hylemTime,
      itemType: "Gap",
      chronSort: sortValue,
      storySort: sortValue
    })

    alert('Created GAP successfully');

    window.location.reload();
  } catch {
    onError('Failed to add gap');
  }
}

// Edit gap
export async function editGap(gapId, gap, onError) {
  try {
    // Edit GAP
    await api.put(removeApiTxt(gapId), gap);

    alert('Edited GAP successfully')

    window.location.reload()
  } catch {
    onError('Failed to edit gap');
  }
}
