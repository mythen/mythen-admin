import server, { removeApiTxt } from '../server/api';

// Delete any kind of object inside hylem seq
// by passing object element
export async function deleteAnyKind(objEl, setData, hylemSequenceItems) {
  // eslint-disable-next-line no-restricted-globals
  const res = confirm(`Are you sure you want to delete this ${objEl['@type']}?`);
  const sequenceItems = hylemSequenceItems;
  const chronSortDeleting = objEl.chronSort;
  const storySortDeleting = objEl.storySort;

  if (!res) return;
  try {
    // Update Backend
    for (const el of sequenceItems) {
      const elChronSort = await server.get(removeApiTxt(el.hylemSequenceItems[0]))
        .then((result) => result.data['chronSort']);
      const elStorySort = await server.get(removeApiTxt(el.hylemSequenceItems[0]))
        .then((result) => result.data['storySort']);
      if (elChronSort > chronSortDeleting) {
        try {
          await server.put(removeApiTxt(el.hylemSequenceItems[0]), {
            'chronSort': elChronSort-1
          })
        } catch {
          alert('Failed to update chronSort positions after deletion !');
        }
      }
      if (elStorySort > storySortDeleting) {
        try {
          await server.put(removeApiTxt(el.hylemSequenceItems[0]), {
            'storySort': elStorySort-1
          })
        } catch {
          alert('Failed to update storySort positions after deletion !');
        }
      }
    }
    await server.delete(removeApiTxt(objEl.hylemSequenceItems[0]));
    // Update Frontend
    setData((prev) => prev.filter((el) => el['@id'] !== objEl['@id']));
    window.location.reload()
  } catch {
    alert('Failed to delete, please try again later !');
  }
}

// Update Position item ..
export async function updatePosition(chronSort, firstEl, secondEl, setData) {
  try {
    const keySort = chronSort ? 'chronSort' : 'storySort'
    // Backend
    await server.put(removeApiTxt(firstEl.hylemSequenceItems[0]), {
      [keySort]: secondEl[keySort]
    })
    await server.put(removeApiTxt(secondEl.hylemSequenceItems[0]), {
      [keySort]: firstEl[keySort]
    })
    // // Frontend
    setData((prev) => prev.map((h) => {
      switch (h['@id']) {
        case firstEl['@id']:
          return { ...h, [keySort]: secondEl[keySort] };
        case secondEl['@id']:
          return { ...h, [keySort]: firstEl[keySort] };
        default:
          return h;
      }
    }))
  } catch {
    alert('Failed to update position !');
  }
}
