import axios from 'axios';
import jwtDecode from 'jwt-decode';
import { site } from '../providers/authProvider';

export default class TokenService {
  refEndpoint = `${site}/v1/token/refresh`;
  availableMin; // EX: After 60 mins will refresh again

  constructor(availableMin = 60, token, refToken) {
    if (token && refToken) tokenDetails.set(token, refToken);

    this.availableMin = availableMin;
    this.updateToken();
  };

  // Update current token, refresh token
  updateToken() {
    const { token, refresh_token } = tokenDetails.get();

    if (token && refresh_token) {
      axios.post(this.refEndpoint, { refresh_token })
        .then(({ data }) => {
          const { token, refresh_token } = data;

          tokenDetails.set(token, refresh_token);
          this.refresh();
        })
        .catch((err) => {
          // In Firefox, request is being cancelled/aborted and throws an error.
          // Chrome doesn't throw any error when request is being cancelled/aborted.
          if (err.message!=='Request aborted') {
            tokenDetails.remove();
          }
        });
    } else {
      tokenDetails.remove();
    }
  };

  // refresh depends on available mins
  refresh() {
    setTimeout(() => {
      this.updateToken();
    }, 1000 * 60 * this.availableMin);
  }

  // remove token, refresh token
  static remove() {
    tokenDetails.remove();
  }

}

// Manuplate with Token, Refresh token
const tokenDetails = {
  set(token, refToken) {
    localStorage.setItem('token', token);
    localStorage.setItem('refresh_token', refToken);
  },

  get() {
    return {
      token: localStorage.getItem('token') || null,
      refresh_token: localStorage.getItem('refresh_token') || null,
    };
  },

  remove() {
    localStorage.removeItem('token');
    localStorage.removeItem('refresh_token');

    window.location.hash = '#/login';
  },
};

// Decode token
export function isAdmin() {
  const token = localStorage.getItem('token');

  if (!token) return false;

  const data = jwtDecode(token);

  return data.roles.includes('ROLE_ADMIN');
}
