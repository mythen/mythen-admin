import server, { removeApiTxt } from '../server/api';

// Determinations
export const determinationsMap = [
  {
    label: 'Subject determination',
    listName: 'subjects',
    keyDet: 'subject',
    detList: 'subjectDeterminations',
    detText: 'subjectDetermination',
    apiDeter: '/subject_determinations',
  },
  {
    label: 'Predicate determination',
    listName: 'predicates',
    keyDet: 'predicate',
    detList: 'predicateDetermination',
    detText: 'predicateDetermination',
    apiDeter: '/predicate_determinations',
    single: true
  },
  {
    label: 'Hylem Object determination',
    listName: 'hylem_objects',
    keyDet: 'hylemObject',
    detList: 'hylemObjectDeterminations',
    detText: 'hylemObjectDetermination',
    apiDeter: '/hylem_object_determinations',
  },
];

// Fetch single dterminaiton
export async function fetchSingleDetermination(api, onSuccess) {
  const { data } = await server.get(removeApiTxt(api));

  onSuccess(data);
}

// Modify Add/Update Determination
export async function modifyDeterminations(deters, onError, onSuccess) {
  try {
    const addedDeterminations = {};
    const deletedDeterminations = [];

    for (const [api, values] of Object.entries(deters)) {
      if (api.includes("/api/")) {
        // for edit
        if (values.deleted === 'true') {
          // for delete
          deletedDeterminations.push(api);
        } else {
          // for edit
          await server.put(removeApiTxt(api), values);
        }
      } else {
        // for add
        const { detList, single } = determinationsMap.find((item) => item.apiDeter === api)
        addedDeterminations[detList] = []
        for (const value of values) {
          const res = await server.post(api, value);
          if (single) {
            addedDeterminations[detList] = res.data['@id']
          } else {
            addedDeterminations[detList].push(res.data['@id'])
          }
        }
      }
    }
    onSuccess(addedDeterminations, deletedDeterminations);
  } catch {
      onError('Failed to modify determinations !');
  }
}

// Add hylem time
export async function addHylemTime(hylemTime, hylem, detrs, selHylem, sortValue, onError) {
  try {
    // // Add Hylem
    const { data: hylemData } = await server.post('/hylems', { ...hylem, ...detrs, chronSort: selHylem.chronSort })
    // Add Hylem Sequence  -> hylem, hylem time
    await server.post('/hylem_sequence_items', {
      hylem: hylemData['@id'],
      itemsTime: hylemTime,
      itemType: 'Hylem',
      chronSort: sortValue,
      storySort: sortValue
    })

    alert('Created hylem successfully, it is added to end of the list.')
    window.location.reload()
  } catch {
    onError('Failed to add hylem !');
  }
}

// Update hylem time
export async function updateHylemTime(hylemId, hylem, hylemData, newDetrs, onError, deletedDeters = []) {
  const finalDeters = {};

  determinationsMap.forEach(({ detList, single }) => {
    if (single) {
      const det = newDetrs[detList] || hylem[detList];

      if (!deletedDeters.includes(det) && det) {
        finalDeters[detList] = det
      }
      else {
        finalDeters[detList] = null
      }
    } else {
      const prevDet = hylem[detList] || [];
      const newDet = newDetrs[detList] || [];
      const finalPrev = prevDet.filter((d) => !deletedDeters.includes(d));

      finalDeters[detList] = [...finalPrev, ...newDet]
    }
  })
  if (finalDeters.subjectDeterminations.length > 0 && finalDeters.predicateDetermination !== null) {
    try {
      // edit Hylem
      await server.put(removeApiTxt(hylemId), { ...hylemData, ...finalDeters })
      window.location.reload()
    } catch {
      onError('Failed to edit hylem !');
    }
  } else {
    alert('Cannot update Hylem. Minimum requirements: 1 Subject and 1 Predicate')
  }
}

// Seperate determinations/hylem data
export function seperateData(data) {
  const hylemData = {};
  const deterData = {};

  Object.keys(data).forEach((key) => {
    if (key[0] === "/") {
      deterData[key] = data[key]
    } else {
      hylemData[key] = data[key]
    }
  })

  return { hylemData, deterData };
}
