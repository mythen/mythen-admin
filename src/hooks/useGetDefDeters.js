import api, { removeApiTxt } from '../server/api';
import { useEffect, useState } from 'react';

export const useGetDefDeter = ({ hylem, keyDet, detList, apiDeter, detText }) => {
  const [defDeters, setDefDeters] = useState([]);
  // const updateDefDeter = (value) => setDefDeters((prev) => [...prev, value]);

  useEffect(() => {
    if (hylem) {
      async function getDefD() {
        const apis = hylem[detList];
        const targetLink = typeof apis == 'string' ? [apis] : apis;
        if (targetLink) {
          const arrObj = [];

          for (const apiLink of targetLink) {
            const { data: det } = await api.get(removeApiTxt(apiLink));

            arrObj.push({
              api: apiLink,
              [keyDet]: det[keyDet]['@id'],
              [detText]: det[detText],
              deleted: false,
            });
          }

          setDefDeters(arrObj);
        }
      }

      getDefD();
    }
  }, [hylem, keyDet, detList, apiDeter, detText]);

  return { defDeters };
};
