import api, { removeApiTxt } from '../server/api';
import { useEffect, useState } from 'react';

// Get nested api just pass array of hylem time/gaps/headline api
export const useGetNestedApi = ({ array, isOnce = false, sorting = false, lazyLoading = false }) => {
  const [once, setOnce] = useState(true);
  const [loading, setLoading] = useState(false);
  const [finalData, setFinalData] = useState([]);

  useEffect(() => {
    if (isOnce && !once) return;

    isOnce && setOnce(false);

    async function fetchHylem() {
      for (const apiLink of array) {
        const { data } = await api.get(removeApiTxt(apiLink));
        for (const nKey of ['hylem', 'gap', 'headline']) {
          if (data[nKey]) {
            const { data: nested } = await api.get(removeApiTxt(data[nKey]));

            const finalObj = sorting ?
              { ...nested, storySort: data['storySort'], chronSort: data['chronSort'] }
              : nested;

            setFinalData((prev) => prev ? [...prev, finalObj] : [finalObj]);
          }
        }
      }

      setLoading(false);
    };

    !lazyLoading && setLoading(true);
    setFinalData([]);
    fetchHylem();
  }, [array, isOnce, once, sorting, lazyLoading]);

  return {
    finalData,
    setFinalData,
    loading,
  };
};
