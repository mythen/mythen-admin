import server from '../server/api';
import { useEffect, useState } from 'react';

// created for fetch all list such subjects, predicates, ..
export const useFetchList = (name, key) => {
  const [list, setList] = useState(null);

  useEffect(() => {
    fetchList({ name, key }, setList);
  }, [name, key]);

  return list;
};

// Fetch list ex: get all subjects, ..
async function fetchList({ name, key }, callback) {
  const { data } = await server.get(`/${name}?itemsPerPage=10000000`);
  callback(data['hydra:member'].map((el) => ({
    name: el[key],
    value: el['@id'],
  })));
}
