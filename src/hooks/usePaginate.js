import { InputLabel, MenuItem, Select } from '@material-ui/core';
import Pagination from '../components/UI/pagination/Pagination';
import { paginate } from '../components/UI/pagination/Pagination';
import React, { useEffect, useState } from 'react';
import { useTranslate } from 'react-admin';

export const usePaginate = (arr) => {
  const [itemPerPage, setItemPerPage] = useState(10);
  const [pagination, setPagination] = useState(1);
  const [items, setItems] = useState([]);
  const translate = useTranslate();

  useEffect(() => {
    setItems(() => [...paginate(arr, pagination, itemPerPage)]);
  }, [arr, pagination, itemPerPage]);

  return {
    items,
    pagination,
    selectPerPageComp: () => <SelectPerPage perPage={itemPerPage} setPerPage={setItemPerPage} translate={translate} setPage={setPagination} />,
    paginateComponent: () => <Pagination
      total={Math.ceil(arr.length / itemPerPage)}
      active={pagination}
      handle={setPagination}
    />,
  };
};

const SelectPerPage = ({ perPage, setPerPage, translate, setPage }) => (
  <div className={'items-per-page-select-menu'} style={{opacity: 0}}>
    <InputLabel shrink>
      {translate(`specialLabels.itemsPerPage`)}
    </InputLabel>
    <Select
      label=""
      value={perPage}
      onChange={(e) => {
        setPerPage(e.target.value);
        setPage(1);
        const paginators = document.querySelectorAll('.paginator');
        const menuItemsPerPage = document.querySelector('.items-per-page-select-menu');
        menuItemsPerPage.style.opacity = 0;
        for (let paginator of paginators) {
          paginator.style.opacity = 0;
        }
      }}
    >
      {[5, 10, 20, 30, 40].map((n, i) => <MenuItem key={`ixa${i}`} value={n}>{n}</MenuItem>)}
    </Select>
  </div>
);
