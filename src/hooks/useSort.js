import { FormControlLabel, Switch } from '@material-ui/core';
import React, { useState } from 'react';
import { useTranslate } from 'react-admin';

export const useSort = (hylems) => {
  const [chronSort, setChronSort] = useState(true);
  const translate = useTranslate();
  const SortOrder = () => <FormControlLabel
    control={<Switch checked={chronSort} onChange={(e) => setChronSort(e.target.checked)} />}
    label={chronSort ? translate(`specialLabels.chronSort`) : translate(`specialLabels.storySort`)}
  />;

  // Sort hylems
  const sortedArr = hylems.sort((a, b) => {
    const keySort = chronSort ? 'chronSort' : 'storySort';

    return a[keySort] - b[keySort];
  });

  return { chronSort, sortedArr, SortOrder };
};
