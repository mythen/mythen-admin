import {useEffect, useState} from 'react';
import {site} from "../providers/authProvider";

// Compare the IDs of the related items to remove duplicates from the array
// eslint-disable-next-line no-extend-native
Array.prototype.unique = function() {
  var a = this;
  for(let i=0; i<a.length; ++i) {
    for(let j=i+1; j<a.length; ++j) {
      if(a[i].id === a[j].id)
        a.splice(j--, 1);
    }
  }
  return a;
};

export const useFetchManyToManyList = (name, srcKey, destKey, recordId) => {
  const [list, setList] = useState(null);
  // eslint-disable-next-line no-unused-vars
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    fetchManyToManyList(name, srcKey, destKey, recordId, setList, setLoading);
  }, [name, srcKey, destKey, recordId, setList, setLoading]);

  return list;
}

export async function fetchManyToManyList(name, srcKey, destKey, recordId, callback, setLoading) {
  let relatedItems = [];

  fetch(`${site}/api/${name}?itemsPerPage=10000000&${srcKey}=${recordId}`, {
    method: 'GET',
    headers: new Headers({
      'Authorization': 'Bearer '+localStorage.getItem('token'),
      'Content-Type': 'application/ld+json'
    })
  })
    .then(response => {
      setLoading(true);
      if (response.ok)
        return response.json();
      else
        throw new Error('Could not load related hylem sequences');
    })
    .then(responseJson => {
      setLoading(true);
      for (let member of responseJson['hydra:member']) {
        for (let relatedItem of member[destKey]) {
          fetch(`${site}${relatedItem}`, {
            method: 'GET',
            headers: new Headers({
              'Authorization': 'Bearer '+localStorage.getItem('token'),
              'Content-Type': 'application/ld+json'
            })
          })
            .then(responseInner => {
              setLoading(true);
              if (responseInner.ok)
                return responseInner.json();
              else
                throw new Error(`Could not load related ${destKey}`);
            })
            .then(responseInnerJson => {
              setLoading(true);
              relatedItems.push(responseInnerJson);
              return relatedItems;
            })
            .then(rel => {
              setLoading(true);
              rel.unique();
              setLoading(false);
              return rel;
            })
            .catch(error => {
              setLoading(false);
              return error;
            });
        }
      }
    })
    .catch(error => {
      setLoading(false);
      return error;
    });

  callback(
    relatedItems
  );
}
