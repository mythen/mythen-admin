import { AUTH_LOGIN, AUTH_LOGOUT, AUTH_ERROR, AUTH_CHECK } from 'react-admin';
import server from '../server/api';
import TokenService from '../services/token.service';

export const site = process.env.REACT_APP_ENDPOINT;

export default async (type, params) => {
  switch (type) {
    case AUTH_LOGIN:
      const { username, password } = params;
      try {
        const { data } = await server.post(`${site}/v1/authentication_token`, { username, password }, { headers: { 'Content-Type': 'application/json' } });
        // RefreshToken.store(data.token, data.refresh_token);
        new TokenService(60, data.token, data.refresh_token);

        window.location.reload();
      } catch (err) {
        throw new Error(err.statusText);
      }

      break;

    case AUTH_LOGOUT:
      TokenService.remove();
      break;

    case AUTH_ERROR:
      if (401 === params.status || 403 === params.status) {
        TokenService.remove();

        return Promise.reject();
      }

      break;

    case AUTH_CHECK:
      return localStorage.getItem('token') ? Promise.resolve() : Promise.reject();

    default:
      return Promise.resolve();
  }
};

// Start Refresh token
new TokenService(60);
