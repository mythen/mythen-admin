import {
  hydraDataProvider as baseHydraDataProvider,
  fetchHydra as baseFetchHydra
} from '@api-platform/admin';
import parseHydraDocumentation from '@api-platform/api-doc-parser/lib/hydra/parseHydraDocumentation';
import React from 'react';
import { Redirect, Route } from 'react-router-dom';
import api, { removeApiTxt } from '../server/api';

const entrypoint = process.env.REACT_APP_HYDRA_ADMIN_ENDPOINT;

const fetchHeaders = () => ({
  Authorization: `Bearer ${localStorage.getItem('token')}`,
});

const fetchHydra = (url, options = {}) =>
  localStorage.getItem('token') ? baseFetchHydra(url, {
    ...options,
    headers: new Headers(fetchHeaders()),
  })
    : baseFetchHydra(url, options);

const apiDocumentationParser = (entrypoint) => {
  return parseHydraDocumentation(
    entrypoint,
    localStorage.getItem('token')
      ? { headers: new Headers(fetchHeaders()) }
      : {}
  ).then(
    ({ api }) => {
      return ({ api });
    },

    (result) => {
      if (result.status === 401) {
        // Prevent infinite loop if the token is expired
        localStorage.removeItem('token');

        return Promise.resolve({
          api: result.api,
          customRoutes: [
            <Route path="/" render={() => {
              return localStorage.getItem('token')
                ? setTimeout(() => {
                  window.location.reload();
                }, 3000)
                : <Redirect to="/login" />;
            }} />,
          ],
        });
      }

      return Promise.reject(result);
    },
  );
};

const semiDataProvider = baseHydraDataProvider(
  entrypoint,
  fetchHydra,
  apiDocumentationParser,
  true
);

// Override the current DataProvider
const dataProvider = {
  ...semiDataProvider,
  update: (_, params) => {
    const { id, data } = params;
    let finalData = {};
    for (const key of Object.keys(data)) {
      if (typeof data[key] != 'object' || id.includes('/api/hylem_sequences/')) {
        finalData[key] = data[key];
      }
    }

    const apiUrl = removeApiTxt(`${entrypoint}${id}`);

    return api.put(apiUrl, finalData);
  },
};

export { entrypoint, dataProvider };
