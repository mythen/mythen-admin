import React from 'react';
import {
  FunctionField,
  Create,
  Show,
  SimpleForm,
  TextInput,
  TextField,
  EditButton,
  ListButton,
  SimpleShowLayout,
  ReferenceArrayField,
  SingleFieldList,
  ChipField,
  required
} from 'react-admin';
import { EditGuesser, FieldGuesser, ListGuesser } from '@api-platform/admin';
import { EditTitle, ShowTitle } from './util/Tools';
import { isAdmin } from '../services/token.service';

export const LocalityCreate = (props) => (
  <Create {...props}>
    <SimpleForm>
      <TextInput source="placeName" validate={[required()]} />
      <TextInput source="wikidataID" />
      <TextInput source="gettyID" />
    </SimpleForm>
  </Create>
);

export const LocalityEdit = (props) => (
  <EditGuesser {...props} title={<EditTitle />}>
    <TextInput source="placeName" validate={[required()]} />
    <TextInput source="wikidataID" />
    <TextInput source="gettyID" />
  </EditGuesser>
);

export const LocalityList = (props) => (
  <ListGuesser {...props} perPage={25} sort={{ field: 'id', order: 'DESC' }} bulkActionButtons={isAdmin()}>
    <FunctionField
      label="ID"
      sortBy="id"
      render={record => (record.id).replace('/api/source_localities/', '')}
    />
    <FieldGuesser source="placeName" />
  </ListGuesser>
);

export const LocalityShow = (props) => (
  <Show {...props} title={<ShowTitle />}>
    <SimpleShowLayout>
      <TextField source="placeName" />
      <TextField source="wikidataID" />
      <TextField source="gettyID" />
      <ReferenceArrayField
        source="sources"
        reference="sources">
        <SingleFieldList>
          <ChipField source="title" />
        </SingleFieldList>
      </ReferenceArrayField>
      {isAdmin() && <EditButton style={{ marginTop: 40 }} />}
      <ListButton style={{ marginTop: 20 }} />
    </SimpleShowLayout>
  </Show>
);
