import {
  EditGuesser,
  FieldGuesser,
  InputGuesser,
  ListGuesser,
} from '@api-platform/admin';
import React from 'react';
import { SimpleShowLayout, Show } from 'react-admin';

const ShowHylemObject = ({ record }) => <p>{record.hylemObject.hylemObject}</p>;

export const HylemObjectDeterminationList = props => (
  <ListGuesser {...props} perPage={25}>
    <FieldGuesser source={'hylem'} />
    <ShowHylemObject label="Hylem object" />
    <FieldGuesser source={'hylemObjectDetermination'} />
  </ListGuesser>
);

export const HylemObjectDeterminationShow = props => (
  <Show {...props}>
    <SimpleShowLayout>
      <FieldGuesser source={'hylem'} addLabel={true} />
      <FieldGuesser source={'hylemObject'} addLabel={true} />
      <FieldGuesser source={'hylemObjectDetermination'} addLabel={true} />
    </SimpleShowLayout>
  </Show>
);

export const HylemObjectDeterminationEdit = props => (
  <EditGuesser {...props}>
    <InputGuesser source={'hylem'} />
    <InputGuesser source={'hylemObject'} />
    <InputGuesser source={'hylemObjectDetermination'} />
  </EditGuesser>
);
