import React from 'react';
import {
  EditGuesser,
  FieldGuesser,
  InputGuesser,
  ListGuesser
} from '@api-platform/admin';
import {
  Create,
  FunctionField,
  SimpleForm,
  TextInput,
  ReferenceArrayField,
  SingleFieldList,
  Show,
  SimpleShowLayout,
  required
} from 'react-admin';
import { ShowDeterChip } from '../subject/Subject';
import { EditTitle, ShowTitle } from '../util/Tools';
import { isAdmin } from '../../services/token.service';

export const HylemObjectCreate = (props) => (
  <Create {...props}>
    <SimpleForm>
      <TextInput source="hylemObject" validate={[required()]} />
    </SimpleForm>
  </Create>
);

export const HylemObjectShow = props => (
  <Show {...props} title={<ShowTitle />}>
    <SimpleShowLayout>
      <FieldGuesser source={'hylemObject'} addLabel={true} />
      <ReferenceArrayField
        source="hylemObjectDeterminations"
        reference="hylem_object_determinations"
      >
        <SingleFieldList>
          <ShowDeterChip source="hylemObjectDetermination" />
        </SingleFieldList>
      </ReferenceArrayField>
    </SimpleShowLayout>
  </Show>
);

export const HylemObjectEdit = (props) => (
  <EditGuesser {...props} title={<EditTitle />}>
    <InputGuesser source="hylemObject" validate={[required()]} />
  </EditGuesser>
);

export const HylemObjectList = (props) => (
  <ListGuesser {...props} perPage={25} sort={{ field: 'id', order: 'DESC' }} bulkActionButtons={isAdmin()}>
    <FunctionField
      label="ID"
      sortBy="id"
      render={record => (record.id).replace('/api/hylem_objects/', '')}
    />
    <FieldGuesser source="hylemObject" />
  </ListGuesser>
);
