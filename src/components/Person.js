import React from 'react';
import {
  Create,
  Edit,
  Show,
  SimpleForm,
  TextInput,
  TextField,
  EditButton,
  ListButton,
  SimpleShowLayout,
  required
} from 'react-admin';
import { EditTitle, ShowTitle } from './util/Tools';
import { FieldGuesser, ListGuesser } from '@api-platform/admin';
import { isAdmin } from '../services/token.service';

export const PersonCreate = (props) => (
  <Create {...props}>
    <SimpleForm>
      <TextInput source="name" validate={[required()]} />
      <TextInput source="wikidataID" />
      <TextInput source="gndID" />
    </SimpleForm>
  </Create>
);

export const PersonEdit = (props) => (
  <Edit {...props} title={<EditTitle />}>
    <SimpleForm>
      <TextInput source="name" validate={[required()]} />
      <TextInput source="wikidataID" />
      <TextInput source="gndID" />
    </SimpleForm>
  </Edit>
);

export const PersonList = (props) => (
  <ListGuesser {...props} sort={{ field: 'id', order: 'DESC' }} perPage={25} bulkActionButtons={isAdmin()}>
    <FieldGuesser source="name" />
  </ListGuesser>
);

export const PersonShow = (props) => (
  <Show {...props} title={<ShowTitle />}>
    <SimpleShowLayout>
      <TextField source="name" />
      <TextField source="wikidataID" />
      <TextField source="gndID" />
      <EditButton />
      <ListButton />
    </SimpleShowLayout>
  </Show>
);
