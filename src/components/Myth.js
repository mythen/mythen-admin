import React from 'react';
import {
  ListGuesser,
  FieldGuesser,
  InputGuesser
} from '@api-platform/admin';

import {
  Create,
  Edit,
  FunctionField,
  SimpleForm,
  TextInput,
  Show,
  SimpleShowLayout,
  ListButton,
  ReferenceArrayField,
  SingleFieldList,
  ChipField,
  required
} from 'react-admin';
import { EditTitle, ShowTitle } from './util/Tools';
import { isAdmin } from '../services/token.service';
import { TextArrayField } from './util/TextArrayField';

export const MythCreate = (props) => (
  <Create {...props}>
    <SimpleForm>
      <TextInput source="title" validate={[required()]} >Mythos</TextInput>
    </SimpleForm>
  </Create>
);

export const MythEdit = (props) => (
  <Edit {...props} title={<EditTitle />}>
    <SimpleForm>
      <InputGuesser source="title" validate={[required()]} />
    </SimpleForm>
  </Edit>
);

export const MythShow = (props) => {
  return (
    <Show {...props} title={<ShowTitle/>}>
      <SimpleShowLayout>
        <FieldGuesser source={'title'} addLabel={true}/>
        <TextArrayField srcKey={'myths'} destKey={'sources'} label={'specialLabels.sources'}>
          <SingleFieldList>
            <ChipField source="title"/>
          </SingleFieldList>
        </TextArrayField>
        <ReferenceArrayField source="hylemSequences" reference="hylem_sequences">
          <SingleFieldList>
            <ChipField source="name"/>
          </SingleFieldList>
        </ReferenceArrayField>
        <ListButton/>
      </SimpleShowLayout>
    </Show>
  );
};

export const MythList = (props) => (
  <ListGuesser {...props} perPage={25} sort={{ field: 'id', order: 'DESC' }} bulkActionButtons={isAdmin()}>
    <FunctionField
      label="ID"
      sortBy="id"
      render={record => (record.id).replace('/api/myths/', '')}
    />
    <FieldGuesser source="title" />
  </ListGuesser>
);
