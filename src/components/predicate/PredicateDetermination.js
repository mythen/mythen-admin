import {
  EditGuesser,
  FieldGuesser,
  InputGuesser,
  ListGuesser,
} from '@api-platform/admin';
import React from 'react';
import { Show, SimpleShowLayout } from 'react-admin';

const ShowPredicate = ({ record }) => <p>{record.predicate.predicate}</p>;

export const PredicateDeterminationList = props => (
  <ListGuesser {...props} perPage={25}>
    <FieldGuesser source={'hylem'} />
    <ShowPredicate label="Predicate" />
    <FieldGuesser source={'predicateDetermination'} />
  </ListGuesser>
);

export const PredicateDeterminationShow = props => (
  <Show {...props}>
    <SimpleShowLayout>
      <FieldGuesser source={'hylem'} addLabel={true} />
      <ShowPredicate label="Predicate" />
      <FieldGuesser source={'predicateDetermination'} addLabel={true} />
    </SimpleShowLayout>
  </Show>
);

export const PredicateDeterminationEdit = props => (
  <EditGuesser {...props}>
    <InputGuesser source={'hylem'} />
    <InputGuesser source={'predicate'} />
    <InputGuesser source={'predicateDetermination'} />
  </EditGuesser>
);
