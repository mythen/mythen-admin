import {
  CreateGuesser,
  EditGuesser,
  FieldGuesser,
  InputGuesser,
  ListGuesser,
} from '@api-platform/admin';
import {
  FunctionField,
  ReferenceArrayField,
  Show,
  SimpleShowLayout,
  SingleFieldList,
  required
} from 'react-admin';
import React from 'react';
import { ShowDeterChip } from '../subject/Subject';
import { EditTitle, ShowTitle } from '../util/Tools';
import { isAdmin } from '../../services/token.service';

export const PredicateCreate = (props) => (
  <CreateGuesser {...props}>
    <InputGuesser source="predicate" validate={[required()]} />
  </CreateGuesser>
);

export const PredicateEdit = (props) => (
  <EditGuesser {...props} title={<EditTitle/>}>
    <InputGuesser source="predicate" validate={[required()]} />
  </EditGuesser>
);

export const PredicateShow = props => (
  <Show {...props} title={<ShowTitle />}>
  {/*<Show {...props} title={<ShowTitle />}>*/}
    <SimpleShowLayout>
      <FieldGuesser source={'predicate'} addLabel={true} />
      <ReferenceArrayField source="predicateDeterminations" reference="predicate_determinations" >
        <SingleFieldList>
          <ShowDeterChip source="predicateDetermination" />
        </SingleFieldList>
      </ReferenceArrayField>
    </SimpleShowLayout>
  </Show>
);

export const PredicateList = (props) => (
  <ListGuesser {...props} perPage={25} sort={{ field: 'id', order: 'DESC' }} bulkActionButtons={isAdmin()}>
    <FunctionField
      label="ID"
      sortBy="id"
      render={record => (record.id).replace('/api/predicates/', '')}
    />
    <FieldGuesser source="predicate" />
  </ListGuesser>
);
