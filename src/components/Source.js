import React from 'react';
import {
  ListGuesser,
  FieldGuesser
} from '@api-platform/admin';
import {
  Create,
  Edit,
  FunctionField,
  SimpleForm,
  TextInput,
  ReferenceInput,
  SelectInput,
  Show,
  SimpleShowLayout,
  TextField,
  ReferenceArrayField,
  SingleFieldList,
  ChipField,
  required
} from 'react-admin';
import InputGuesser from '@api-platform/admin/lib/InputGuesser';
import { EditTitle, ShowTitle } from './util/Tools';
import { isAdmin } from '../services/token.service';
import { TextArrayField } from './util/TextArrayField';

export const SourceCreate = (props) => (
  <Create {...props}>
    <SimpleForm>
      <InputGuesser source="title" validate={[required()]} />
      <InputGuesser source="titleShort" />
      <TextInput source="referenceUrl" />
      <TextInput source="referenceText" validate={[required()]} />
      <ReferenceInput
        source="locality"
        reference="source_localities">
        <SelectInput optionText="placeName" />
      </ReferenceInput>
      <TextInput source="localityContext" />
      <TextInput source="language" />
      <ReferenceInput
        source="objectType"
        reference="source_objects">
        <SelectInput optionText="objectType" validate={[required()]} />
      </ReferenceInput>
      <TextInput source="genre" />
      <InputGuesser source="contextualization" />
    </SimpleForm>
  </Create>
);

export const SourceEdit = (props) => (
  <Edit {...props} title={<EditTitle />}>
    <SimpleForm>
      <InputGuesser source="title" validate={[required()]} />
      <InputGuesser source="titleShort" />
      <TextInput source="referenceUrl" />
      <TextInput source="referenceText" validate={[required()]} />
      <ReferenceInput
        source="locality"
        reference="source_localities">
        <SelectInput optionText="placeName" />
      </ReferenceInput>
      <TextInput source="localityContext" />
      <TextInput source="language" />
      <ReferenceInput
        source="objectType"
        reference="source_objects">
        <SelectInput optionText="objectType" validate={[required()]} />
      </ReferenceInput>
      <TextInput source="genre" />
      <InputGuesser source="contextualization" />
    </SimpleForm>
  </Edit>
);

export const SourceShow = props => {
  return (
    <Show {...props} title={<ShowTitle/>}>
      <SimpleShowLayout>
        <FieldGuesser source={'title'} addLabel={true}/>
        <FieldGuesser source={'titleShort'} addLabel={true}/>
        <FieldGuesser source={'referenceUrl'}/>
        <FieldGuesser source={'referenceText'} addLabel={true}/>
        <TextField source="date.textual"/>
        <FieldGuesser source={'locality'} addLabel={true}/>
        <FieldGuesser source={'localityContext'} addLabel={true}/>
        <FieldGuesser source={'language'} addLabel={true}/>
        <FieldGuesser source={'objectType'} addLabel={true}/>
        <FieldGuesser source={'genre'} addLabel={true}/>
        <FieldGuesser source={'contextualization'} addLabel={true}/>
        <TextArrayField srcKey={'sources'} destKey={'myths'} label={'specialLabels.myths'}>
          <SingleFieldList>
            <ChipField source="id"/>
          </SingleFieldList>
        </TextArrayField>
        <ReferenceArrayField source="hylemSequences" reference="hylem_sequences">
          <SingleFieldList>
            <ChipField source="name"/>
          </SingleFieldList>
        </ReferenceArrayField>
        <FieldGuesser source={'created'} addLabel={true}/>
        <FieldGuesser source={'modified'} addLabel={true}/>
        <FieldGuesser source={'editor'} addLabel={true}/>
      </SimpleShowLayout>
    </Show>
  );
};

export const SourceList = (props) => (
  <ListGuesser {...props} perPage={25} sort={{ field: 'id', order: 'DESC' }} bulkActionButtons={isAdmin()}>
    <FunctionField
      label="ID"
      sortBy="id"
      render={record => (record.id).replace('/api/sources/', '')}
    />
    <FieldGuesser source="title" />
    <FieldGuesser source="titleShort" />
    <FieldGuesser source="language" />
    <FieldGuesser source="genre" />
  </ListGuesser>
);
