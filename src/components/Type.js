import React from 'react';
import {
  FieldGuesser,
  ListGuesser,
} from '@api-platform/admin';
import { isAdmin } from '../services/token.service';

import {
  Create,
  Edit,
  FunctionField,
  Show,
  SimpleForm,
  SimpleShowLayout,
  TextInput,
  required
} from 'react-admin';
import { EditTitle, ShowTitle } from './util/Tools';

export const TypeCreate = (props) => (
  <Create {...props}>
    <SimpleForm>
      <TextInput source="type" validate={[required()]} />
    </SimpleForm>
  </Create>
);

export const TypeEdit = (props) => (
  <Edit {...props} title={<EditTitle />}>
    <SimpleForm>
      <TextInput source="type" validate={[required()]} />
    </SimpleForm>
  </Edit>
);

export const TypeShow = props => (
  <Show {...props} title={<ShowTitle />}>
    <SimpleShowLayout>
      <FieldGuesser source={ 'type' } addLabel={true} />
    </SimpleShowLayout>
  </Show>
);

export const TypeList = (props) => (
  <ListGuesser {...props} sort={{ field: 'id', order: 'DESC' }} bulkActionButtons={isAdmin()}>
    <FunctionField
      label="ID"
      sortBy="id"
      render={record => (record.id).replace('/api/types/', '')}
    />
      <FieldGuesser source="type"/>
  </ListGuesser>
);
