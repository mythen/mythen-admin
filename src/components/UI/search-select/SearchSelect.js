import React from 'react';
import SelectSearch from 'react-select-search';
import './SearchSelect.scss';

export default function SearchSelectBox(props) {
  return (
    <SelectSearch search {...props} />
  );
}
