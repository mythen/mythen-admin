import React from 'react';
import PropTypes from 'prop-types';
import SwipeableViews from 'react-swipeable-views';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Box from '@material-ui/core/Box';
import styled from 'styled-components';
import { useTranslate } from 'react-admin';

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`full-width-tabpanel-${index}`}
      aria-labelledby={`full-width-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box p={3}>
          {children}
        </Box>
      )}
    </div>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
};

function a11yProps(index) {
  return {
    id: `full-width-tab-${index}`,
    'aria-controls': `full-width-tabpanel-${index}`,
  };
}

export default function FullWidthTabs({ items, children }) {
  const [value, setValue] = React.useState(0);
  const translate = useTranslate();

  const handleChange = (_, newValue) => {
    setValue(newValue);
  };

  const handleChangeIndex = (index) => {
    setValue(index);
  };

  return (
    <div>
      <Wrapper>
        <Tabs
          value={value}
          onChange={handleChange}
          indicatorColor="secondary"
          variant="fullWidth"
          centered
          aria-label="full width tabs example"
        >
          {items.map((item, i) => (
            <Tab label={translate(`specialLabels.hylemTabs.${item}`)} key={`sh${i}`} {...a11yProps(i)} />
          ))}
        </Tabs>
      </Wrapper>
      <SwipeableViews
        index={value}
        onChangeIndex={handleChangeIndex}
      >
        {children.map((child, i) => (
          <TabPanel value={value} index={i} key={`shh${i}`}>
            {child}
          </TabPanel>
        ))}

      </SwipeableViews>
    </div>
  );
}

const Wrapper = styled.div`
  background: #f6f6f6;
`;
