import React from 'react';
import styles from './Pagination.module.scss';

export default function Pagination({ total, active = 1, handle }) {
  return (
    <ul className={styles.pagination__list + ' paginator'} style={{marginBottom: 10, opacity: 0}}>
      {Array(total).fill(0).map((_, i) => (
        <li
          onClick={() => {
            window.scrollTo(0, 0);
            handle(i + 1);
          }}
          key={`pg${i}`}
          className={active === i + 1 ? styles.active : ''}>{i + 1}
        </li>
      ))}
    </ul>
  );
};

export const paginate = (arr, page, limit) => arr.slice((page - 1) * limit, page * limit);
