import { Checkbox, FormControlLabel } from '@material-ui/core';
import { Controller } from 'react-hook-form';
import React from 'react';
import styled from 'styled-components';

export const CustomInput = ({ label, refer, displayNone, ...rest }) => (
  <Wrapper displayNone={displayNone}>
    <div>{label}</div>
    <input type="text" ref={refer} {...rest} />
  </Wrapper>
);

export const CustomRadio = ({ label, options, refer, empty, ...rest }) => {

  const sortedOptions = options.sort((a, b) => {
    if (String(a.name).toLowerCase() < String(b.name).toLowerCase()) {
      return -1;
    }

    if (String(a.name).toLowerCase() > String(b.name).toLowerCase()) {
      return 1;
    };

    return 0;
  });

  return <Wrapper>
    <div>{label}</div>
    <select ref={refer} {...rest}>
      {empty && <option value=""></option>}
      {sortedOptions.map(({ name, value }) => <option key={value} value={value}>{name}</option>)}
    </select>
  </Wrapper>;
};

// Checkbox
export const CheckBox = ({ name, control, label, defaultValue }) => (
  <Controller
    name={name}
    control={control}
    defaultValue={defaultValue}
    render={props => (<FormControlLabel
      control={
        <Checkbox
          onChange={e => props.onChange(e.target.checked)}
          checked={props.value || false}
        />
      }
      label={label}
    />)}
  />
);

const Wrapper = styled.div`
  ${({ displayNone }) => displayNone && 'display: none;'}
  > :first-child {
      margin-bottom: 10px;
  }
`;
