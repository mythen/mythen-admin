import Alert from '@material-ui/lab/Alert';
import { Button, CircularProgress, DialogActions } from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';
import Dialog from '@material-ui/core/Dialog';
import IconButton from '@material-ui/core/IconButton';
import MuiDialogContent from '@material-ui/core/DialogContent';
import MuiDialogTitle from '@material-ui/core/DialogTitle';
import React from 'react';
import Typography from '@material-ui/core/Typography';
import { withStyles } from '@material-ui/core/styles';
import { useTranslate } from 'react-admin';

const styles = (theme) => ({
  closeButton: {
    color: theme.palette.grey[500],
    position: 'absolute',
    right: theme.spacing(1),
    top: theme.spacing(1),
  },
  root: {
    margin: 0,
    minWidth: '500px',
    padding: theme.spacing(2),
  },
});

const DialogContent = withStyles((theme) => ({
  root: {
    padding: theme.spacing(2),
  },
}))(MuiDialogContent);

export default function CustomizedDialogs(
  { open, close, loading, error, title, children, okHandle, ...props }
) {
  const translate = useTranslate();

  return (
    <Dialog disableBackdropClick={loading} open={open} onClose={close} {...props}>
      <DialogTitle id="customized-dialog-title" onClose={close}>
        {title}
      </DialogTitle>

      <DialogContent dividers>
        {error && <Alert severity="error" style={{ marginBottom: 20 }}>{error}</Alert>}
        {!loading ? children : <CircularProgress />}
      </DialogContent>
      {okHandle &&
        <DialogActions>
          {!loading && <Button onClick={okHandle} color="primary">
            {translate(`specialLabels.actions.saveChanges`)}
          </Button>}
        </DialogActions>}
    </Dialog>
  );
}

const DialogTitle = withStyles(styles)((props) => {
  const { children, classes, onClose, ...other } = props;

  return (
    <MuiDialogTitle disableTypography className={classes.root} {...other}>
      <Typography variant="h6">{children}</Typography>
      {onClose ? (
        <IconButton aria-label="close" className={classes.closeButton} onClick={onClose}>
          <CloseIcon />
        </IconButton>
      ) : null}
    </MuiDialogTitle>
  );
});
