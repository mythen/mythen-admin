import {
  EditGuesser,
  FieldGuesser,
  InputGuesser,
  ListGuesser,
} from '@api-platform/admin';
import React from 'react';
import { Show, SimpleShowLayout } from 'react-admin';

const ShowSubject = ({ record }) => <p>{record.subject.subject}</p>;

export const SubjectDeterminationList = props => (
  <ListGuesser {...props} perPage={25}>
    <FieldGuesser source={'hylem'} />
    <ShowSubject label="Subject" />
    <FieldGuesser source={'subjectDetermination'} />
  </ListGuesser>
);

export const SubjectDeterminationShow = props => (
  <Show {...props}>
    <SimpleShowLayout>
      <FieldGuesser source={'hylem'} addLabel={true} />
      <ShowSubject label="Subject" />
      <FieldGuesser source={'subjectDetermination'} addLabel={true} />
    </SimpleShowLayout>
  </Show>
);

export const SubjectDeterminationEdit = props => (
  <EditGuesser {...props}>
    <InputGuesser source={'hylem'} />
    <InputGuesser source={'subject'} />
    <InputGuesser source={'subjectDetermination'} />
  </EditGuesser>
);
