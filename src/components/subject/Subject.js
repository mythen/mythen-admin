import { Chip } from '@material-ui/core';
import {
  Create,
  EditButton,
  FunctionField,
  ListButton,
  ReferenceArrayField,
  Show,
  SimpleForm,
  SimpleShowLayout,
  SingleFieldList,
  TextField,
  TextInput,
  required
} from 'react-admin';
import {
  EditGuesser,
  FieldGuesser,
  InputGuesser,
  ListGuesser
} from '@api-platform/admin';
import { EditTitle, ShowTitle } from '../util/Tools';
import React from 'react';
import { isAdmin } from '../../services/token.service';

export const SubjectCreate = (props) => (
  <Create {...props}>
    <SimpleForm>
      <TextInput source="subject" validate={[required()]} />
    </SimpleForm>
  </Create>
);

export const SubjectEdit = (props) => (
  <EditGuesser {...props} title={<EditTitle />}>
    <InputGuesser source="subject" validate={[required()]} />
  </EditGuesser>
);

export const SubjectList = (props) => (
  <ListGuesser {...props} perPage={25} sort={{ field: 'id', order: 'DESC' }} bulkActionButtons={isAdmin()}>
    <FunctionField
      label="ID"
      sortBy="id"
      render={record => (record.id).replace('/api/subjects/', '')}
    />
    <FieldGuesser source="subject" />
  </ListGuesser>
);

export const ShowDeterChip = ({ record, source }) => record?.[source] ?
  <Chip style={{ margin: 4 }} label={record?.[source]} /> : <></>

export const SubjectShow = (props) => (
  <Show {...props} title={<ShowTitle />}>
    <SimpleShowLayout>
      <TextField source="subject" />
      <ReferenceArrayField
        source="subjectDeterminations"
        reference="subject_determinations"
      >
        <SingleFieldList>
          <ShowDeterChip source={"subjectDetermination"} />
        </SingleFieldList>
      </ReferenceArrayField>
      {isAdmin() && <EditButton style={{ marginTop: 40 }} />}
      <ListButton style={{ marginTop: 40 }} />
    </SimpleShowLayout>
  </Show>
);
