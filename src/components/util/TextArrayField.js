import React from 'react';
import Chip from '@material-ui/core/Chip';
import { useFetchManyToManyList } from "../../hooks/useFetchManyToManyList";

const handleClick = id => e => {
  let newHref = '';
  if (window.location.href.includes('mythen-admin') !== false) {
    newHref = '/mythen-admin/#/'+id+'/show';
  } else {
    newHref = '/#/'+id+'/show';
  }
  window.location.href = newHref;
  window.location.reload();
};

export const TextArrayField = ({ record, srcKey, destKey }) => {
  const array =  useFetchManyToManyList('hylem_sequences', srcKey, destKey, record.id);

  if (typeof array === 'undefined' || array === null || array.length === 0) {
    return <div/>
  } else {
    return (
      <>
        {array.map(item =>
          <Chip
            label={item.title}
            key={item.title}
            clickable={true}
            onClick={handleClick(destKey+'/'+(item['@id'].toString().replaceAll('/', '%2F')))}
            style={{marginRight: 8 + 'px'}}
          />
        )}
      </>
    )
  }
};

TextArrayField.defaultProps = { addLabel: true }
