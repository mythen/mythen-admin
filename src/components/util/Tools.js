import React from 'react';
import { Button } from '@material-ui/core';
import { useUpdate, useTranslate } from 'react-admin';

export const UpdateRes = ({ record, res, resApi, data }) => {
  const [update] =
    useUpdate(res, `api/${resApi}/${record.originId}`,
      JSON.stringify(data),
      record);

  return (
    <Button variant="outlined" color="primary" onClick={update}>Update</Button>
  );
};

// Showing specific id
export const IdShow = ({ record, txt }) => (
  <div>{txt} {record?.originId}</div>
);

// Showing specific title in Show view
export const ShowTitle = ({ record }) => {
  const translate = useTranslate();
  return <span>{record ? translate(`dynamicTitles.title${(record["@type"])}`) : ''} {record ? `#${(record.id).substring((record.id).lastIndexOf("/") + 1)}` : ''}</span>;
};

// Showing specific title in Edit view
export const EditTitle = ({ record }) => {
  const translate = useTranslate();
  return <span>{translate(`dynamicTitles.edit`)} {record ? translate(`dynamicTitles.title${(record["@type"])}`) : ''} {record ? `#${(record.id).substring((record.id).lastIndexOf("/") + 1)}` : ''}</span>;
};

export const CustomProgressBar = ({itemsCountOnPage, currentListLength}) => {
  const loadingBarPercentage = (currentListLength / itemsCountOnPage) * 100;
  return (
    <div className="meter animate nostripes">
      <span style={{width: loadingBarPercentage+'%'}}><span/></span>
    </div>
  )
};
