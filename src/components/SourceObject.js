import React from 'react';
import {
  FunctionField,
  Create,
  Show,
  Edit,
  SimpleForm,
  TextInput,
  TextField,
  EditButton,
  ListButton,
  SimpleShowLayout,
  ReferenceArrayField,
  SingleFieldList,
  ChipField,
  required
} from 'react-admin';
import { EditTitle, ShowTitle } from './util/Tools';
import { FieldGuesser, ListGuesser } from '@api-platform/admin';
import { isAdmin } from '../services/token.service';

export const SourceObjectCreate = (props) => (
  <Create {...props}>
    <SimpleForm>
      <TextInput source="objectType" validate={[required()]} />
    </SimpleForm>
  </Create>
);

export const SourceObjectEdit = (props) => (
  <Edit {...props} title={<EditTitle />}>
    <SimpleForm>
      <TextInput source="objectType" validate={[required()]} />
    </SimpleForm>
  </Edit>
);

export const SourceObjectList = (props) => (
  <ListGuesser {...props} perPage={25} sort={{ field: 'id', order: 'DESC' }} bulkActionButtons={isAdmin()}>
    <FunctionField
      label="ID"
      sortBy="id"
      render={record => (record.id).replace('/api/source_objects/', '')}
    />
    <FieldGuesser source="objectType" />
  </ListGuesser>
);

export const SourceObjectShow = (props) => (
  <Show {...props} title={<ShowTitle />}>
    <SimpleShowLayout>
      <TextField source="objectType" />
      <ReferenceArrayField
        source="sources"
        reference="sources">
        <SingleFieldList>
          <ChipField source="title" />
        </SingleFieldList>
      </ReferenceArrayField>
      {isAdmin() && <EditButton style={{ marginTop: 40 }} />}
      <ListButton style={{ marginTop: 20 }} />
    </SimpleShowLayout>
  </Show>
);
