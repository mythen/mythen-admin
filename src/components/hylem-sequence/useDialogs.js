import AddGapDialog from './gap/AddDialog';
import AddHeadlineDialog from './headline/AddDialog';
import AddHylemTimeDialog from './hylem/AddDialog';
import EditGapDialog from './gap/EditDialog';
import EditHeadlineDialog from './headline/EditDialog';
import EditHylemTime from './hylem/EditDialog';
import React, {useState} from "react"

export const useDialogs = (record) => {
  const [dialogHylem, setDialogHylem] = useState({ add: false, edit: false, selected: {} })
  const [dialogGap, setDialogGap] = useState({ add: false, edit: false, selected: {} })
  const [dialogHeadline, setDialogHeadline] = useState({ add: false, edit: false, selected: {} })

  const hylemSequenceItemsCount = record['itemsTime'].length

  const handleDialogHylem = (name, selected = {}) => setDialogHylem((prev) => ({ ...prev, [name]: !prev[name], selected }))
  const handleDialogGap = (name, selected = {}) => setDialogGap((prev) => ({ ...prev, [name]: !prev[name], selected }))
  const handleDialogHeadline = (name, selected = {}) => setDialogHeadline((prev) => ({ ...prev, [name]: !prev[name], selected }))

  const Dialogs = () => (
    <>
      {/* Edit hylem Dialog */}
      {dialogHylem.edit && <EditHylemTime
        modal={dialogHylem.edit}
        setModal={handleDialogHylem}
        hylemId={dialogHylem.selected['@id']}
      />}

      {/* Add hylem Dialog */}
      {dialogHylem.add && <AddHylemTimeDialog
        hylemTime={record['@id']}
        modal={dialogHylem.add}
        setModal={handleDialogHylem}
        sequenceLength={hylemSequenceItemsCount}
        selHylem={dialogHylem.selected}
      />}

      {/* Edit GAP Dialog */}
      {dialogGap.edit && <EditGapDialog
          modal={dialogGap.edit}
          setModal={handleDialogGap}
          gapId={dialogGap.selected['@id']}
      />}

      {/* Add GAP Dialog */}
      {dialogGap.add && <AddGapDialog
        hylemTime={record['@id']}
        modal={dialogGap.add}
        setModal={handleDialogGap}
        sequenceLength={hylemSequenceItemsCount}
        selHylem={dialogGap.selected}
      />}

      {/* Edit headline Dialog */}
      {dialogHeadline.edit && <EditHeadlineDialog
        modal={dialogHeadline.edit}
        setModal={handleDialogHeadline}
        headlineId={dialogHeadline.selected['@id']}
      />}

      {/* Add headline Dialog */}
      {dialogHeadline.add && <AddHeadlineDialog
        hylemTime={record['@id']}
        modal={dialogHeadline.add}
        setModal={handleDialogHeadline}
        sequenceLength={hylemSequenceItemsCount}
        selHylem={dialogHeadline.selected}
      />}
    </>
  )
  return {
    Dialogs,
    handleDialogHylem,
    handleDialogGap,
    handleDialogHeadline
  };
};
