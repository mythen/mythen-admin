import ArrowDown from '@material-ui/icons/ArrowDownward'
import ArrowUp from '@material-ui/icons/ArrowUpward'
import DeleteIcon from '@material-ui/icons/Delete'
import EditIcon from '@material-ui/icons/Edit';
import GapDetails from './gap/Details';
import HylemDetails from './hylem/Details';
import HeadlineDetails from './headline/Details';
import { IconButton, Tooltip } from '@material-ui/core';
import React, { useRef } from 'react';
import { useDrag, useDrop } from 'react-dnd';
import { useTranslate } from 'react-admin';

export const Item = ({ type, length, data, index, handleDragMove, handleDelete, move, handleDialog, id }) => {
  const handleDialogTypes = { Hylem: 0, Gap: 1, Headline: 2 };
  const handleDialogDetect = handleDialog[handleDialogTypes[type]];
  const ref = useRef(null);
  const translate = useTranslate();
  const [, drop] = useDrop({
    accept: 'card',
    hover(item, monitor) {
      if (!ref.current) {
        return;
      }
      const dragIndex = item.index;
      const hoverIndex = index;
      if (dragIndex === hoverIndex) {
        return;
      }
      const hoverBoundingRect = ref.current?.getBoundingClientRect();
      const hoverMiddleY = (hoverBoundingRect.bottom - hoverBoundingRect.top) / 2;
      const clientOffset = monitor.getClientOffset();
      const hoverClientY = clientOffset.y - hoverBoundingRect.top;

      if (dragIndex < hoverIndex && hoverClientY < hoverMiddleY) {
        return;
      }
      if (dragIndex > hoverIndex && hoverClientY > hoverMiddleY) {
        return;
      }
      handleDragMove(dragIndex, hoverIndex);

      item.index = hoverIndex;
    },
  });
  const [{ isDragging }, drag] = useDrag({
    item: { type: 'card', id, index },
    collect: (monitor) => ({
      isDragging: monitor.isDragging(),
    }),
  });
  const opacity = isDragging ? 0.3 : 1;
  drag(drop(ref));

  return (
    <div ref={ref} style={{ opacity, display: 'none' }} className='sequence-item-container'>
      <div className='sequence-item-header'>
        <div className='sequence-item-type'>{translate(`specialLabels.${type}`)} #{data.id}</div>
        <div className='sequence-item-utility'>
          {index !== 0 && <Tooltip title={translate(`specialLabels.move${type}Up`)}>
            <IconButton
              color={'secondary'}
              variant={'contained'}
              onClick={() => move(data, 'up')}
            >
              <ArrowUp fontSize={'small'} />
            </IconButton>
          </Tooltip>}
          {(length - 1) !== index && <Tooltip title={translate(`specialLabels.move${type}Down`)}>
            <IconButton
              color={'secondary'}
              variant={'contained'}
              onClick={() => move(data, 'down')}
            >
              <ArrowDown fontSize={'small'} />
            </IconButton>
          </Tooltip>}
          <Tooltip title={translate(`specialLabels.delete${type}`)}>
            <IconButton
              color={'secondary'}
              variant={'contained'}
              onClick={() => handleDelete(data)}
            >
              <DeleteIcon fontSize={'small'} />
            </IconButton>
          </Tooltip>
          <Tooltip title={translate(`specialLabels.edit${type}`)}>
            <IconButton
              color={'secondary'}
              variant={'contained'}
              onClick={() => handleDialogDetect('edit', data)}
            >
              <EditIcon fontSize={'small'} />
            </IconButton>
          </Tooltip>
        </div>
      </div>
      {displayContent(data)[type]}
    </div>
  )
}

const displayContent = (data) => ({
  Hylem: <HylemDetails
    basePath='/items'
    id={data['@id']}
    resource="hylems"
  />,
  Headline: <HeadlineDetails
    basePath='/items'
    id={data['@id']}
    resource="headlines"
  />,
  Gap: <GapDetails
    basePath='/items'
    id={data['@id']}
    resource="gaps"
  />,
})
