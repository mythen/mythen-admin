import {
  Show,
  SimpleShowLayout,
  TextField
} from 'react-admin';
import React from 'react';
import styled from 'styled-components';
import { useTranslate } from 'react-admin';

export default (props) => {
  const translate = useTranslate();
  return (
    <Wrapper>
      <Show hasEdit={false} {...props} title={translate(`specialLabels.emptyTitle`)} >
        <SimpleShowLayout>
          <TextField source="gapRemarks" label={'specialLabels.gapRemarks'} />
          <DividerLine />
        </SimpleShowLayout>
      </Show>
    </Wrapper>
  );
}

const Wrapper = styled.div`
  clear: both;
  background: white;
  border: 2px solid #f6f6f6;
  .MuiPaper-elevation1 {
    box-shadow: none !important;
  }
  .MuiBox-root {
    padding: 0 !important;
  }
  .MuiCardContent-root {
    padding-top: 0 !important;
    margin-top: -10px !important;
  }
`;

const DividerLine = styled.p`
  width: 100%;
  height: 1px;
  background-color: #ccc
`;
