import { CustomInput } from '../../UI/Form';
import CustomModal from '../../UI/CustomModal';
import { editGap } from '../../../services/gap.service';
import { LinearProgress, Typography } from '@material-ui/core';
import React, { useRef, useState, } from 'react';
import { removeApiTxt } from '../../../server/api';
import { useForm } from 'react-hook-form';
import useSWR from 'swr';
import { useTranslate } from 'react-admin';

export default function GapTime({ gapId, modal, setModal }) {
  const submitInput = useRef(null);
  const [error, setError] = useState(null);
  const [loading, setLoading] = useState(false);
  const { data } = useSWR(removeApiTxt(gapId));
  const translate = useTranslate();
  const titleString = translate(`dynamicTitles.edit`) + " " + translate(`dynamicTitles.titleGap`) +" #" + gapId.toString().substring((gapId).lastIndexOf("/") + 1);

  return (
    <CustomModal
      className="hylem-dialog"
      title={titleString}
      loading={loading}
      error={error}
      okHandle={() => submitInput.current.click()}
      close={() => setModal('edit')}
      open={modal}
    >
      {
        data ?
          <GapBody
            gap={data}
            gapId={gapId}
            setError={setError}
            setLoading={setLoading}
            submitRef={submitInput}
          />
          : <LinearProgress color="secondary" style={{ width: '200px', margin: '40px auto' }} />
      }
    </CustomModal>
  );
};

const GapBody = ({ gapId, gap, submitRef, setLoading, setError }) => {
  const { register, handleSubmit } = useForm({ defaultValues: { ...gap } }); // for hylem
  // On Submit Form - update GAP
  const handelSubmitForm = handleSubmit(async (data) => {
    setLoading(true);
    editGap(gapId, data, setError);
    setLoading(false);
  });
  const translate = useTranslate();

  return (
    <form onSubmit={handelSubmitForm}>
      <div className="hylem-form">
        {/* Text Reference */}
        <Typography variant="h5">Details</Typography>
        <div className="formedit">
          <CustomInput
            name="gapRemarks"
            label={translate('specialLabels.gapRemarks')+' *'}
            placeholder={translate('specialLabels.yourGapRemarks')}
            refer={register}
            required={true}
          />
        </div>
      </div>
      <input ref={submitRef} type="submit" style={{ display: 'none' }} />
    </form>
  );
};
