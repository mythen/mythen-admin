import {
  Button,
  CircularProgress,
  Menu,
  MenuItem
} from '@material-ui/core';
import Container from '@material-ui/core/Container';
import { deleteAnyKind } from '../../services/hylem_sequence.service';
import { DndProvider } from 'react-dnd';
import { HTML5Backend } from 'react-dnd-html5-backend';
import { Item } from './Item';
import React, { useCallback } from 'react';
import styled from 'styled-components';
import { updatePosition } from '../../services/hylem_sequence.service';
import { usePaginate } from '../../hooks/usePaginate';
import { useSort } from '../../hooks/useSort';
import { useGetNestedApi } from '../../hooks/useGetNestedApi';
import { useDialogs } from './useDialogs';
import { useTranslate } from 'react-admin';
import { Loading } from 'react-admin';
import { CustomProgressBar } from "../util/Tools";

const HylemList = ({ record }) => {
  // For Dialog
  const { Dialogs, handleDialogHylem, handleDialogGap, handleDialogHeadline } = useDialogs(record);

  // Paginations
  const { paginateComponent, selectPerPageComp, items } = usePaginate(record.itemsTime);
  const { finalData: list, setFinalData, loading } = useGetNestedApi({ array: items, sorting: true });
  const { chronSort, SortOrder, sortedArr } = useSort(list);
  const expectedListLength = () => {
    if (document.querySelectorAll('.loading-spinner-overlay')) {
      if (paginateComponent().props.total === 1) {
        return record.itemsTime.length;
      } else if (paginateComponent().props.active < paginateComponent().props.total) {
        return selectPerPageComp().props.perPage;
      } else {
        return record.itemsTime.length % selectPerPageComp().props.perPage;
      }
    }
  }
  const checkIfStillLoading = () => {
    const expectedListLengthThisPage = expectedListLength();
    if (expectedListLengthThisPage === list.length) {
      const itemContainers = document.querySelectorAll('.sequence-item-container');
      const menuItemsPerPage = document.querySelector('.items-per-page-select-menu');
      const paginators = document.querySelectorAll('.paginator');
      if (itemContainers) {
        for (let itemContainer of itemContainers) {
          itemContainer.style.display = 'inherit';
        }
      }
      if (menuItemsPerPage) {
        menuItemsPerPage.style.opacity = 1;
      }
      if (paginators) {
        for (let paginator of paginators) {
          paginator.style.opacity = 1;
        }
      }
    }
  }

  // Drag and drop/ move
  const handleDragMove = useCallback((dragIndex, hoverIndex) => {
    const dragItem = sortedArr[dragIndex];
    const hoverItem = sortedArr[hoverIndex];

    updatePosition(chronSort, dragItem, hoverItem, setFinalData); // update frontend + backend
  }, [chronSort, setFinalData, sortedArr]);
  const [anchorEl, setAnchorEl] = React.useState(null);
  const handleClick = (event) => setAnchorEl(event.currentTarget);
  const handleClose = () => setAnchorEl(null);
  const translate = useTranslate();

  return (
    <DndProvider backend={HTML5Backend}>
      <Container className="hylems">
        {/* Settings / Filter */}
        <Settings style={{marginBottom: 10}}>
          <SortOrder />
          {selectPerPageComp()}
          <CreateNewObject addHylem={handleDialogHylem} addGap={handleDialogGap} addHeadline={handleDialogHeadline} />
        </Settings>
        {/* Pagination: List Top */}
        {paginateComponent()}
        <CustomProgressBar itemsCountOnPage={expectedListLength()} currentListLength={list.length} />
        {/* Display Hylems */}
        {loading && <Loading className='loading-spinner-overlay' />}
        {sortedArr.map((h, idx) => (
          <Item
            type={h['@type']}
            data={h}
            index={idx}
            id={Math.random().toString()}
            handleDelete={(d) => deleteAnyKind(d, setFinalData, list)}
            move={(first, arrow) => arrow === 'down' ?
              updatePosition(chronSort, first, sortedArr[idx + 1], setFinalData)
              : updatePosition(chronSort, sortedArr[idx - 1], first, setFinalData)}
            handleDragMove={handleDragMove}
            key={`det${idx}`}
            handleDialog={[handleDialogHylem, handleDialogGap, handleDialogHeadline]}
            lengthHylems={list.length}
          />
        ))}
        {checkIfStillLoading()}
        {/* In case no hylems */}
        {list.length === 0 &&
          <Button
            onClick={handleClick}
            color="primary"
            variant="outlined"
            fullWidth
            aria-controls="create-objects"
            aria-haspopup="true"
          >
            {translate(`specialLabels.createNewSequenceItem`)}
          </Button>}
          <Menu
            id="simple-menu"
            anchorEl={anchorEl}
            keepMounted
            open={Boolean(anchorEl)}
            onClose={handleClose}
          >
            <MenuItem onClick={() => handleDialogHylem("add")}>{translate(`specialLabels.Hylem`)}</MenuItem>
            <MenuItem onClick={() => handleDialogGap("add")}>{translate(`specialLabels.Gap`)}</MenuItem>
            <MenuItem onClick={() => handleDialogHeadline("add")}>{translate(`specialLabels.Headline`)}</MenuItem>
          </Menu>
        {/* On Loading sequence items */}
        {loading && <CircularProgress />}
        {/* Pagination: List Bottom */}
        {paginateComponent()}
        {/* All Dialogs */}
        <Dialogs />
      </Container>
    </DndProvider>
  );
};

const CreateNewObject = ({ addHylem, addGap, addHeadline }) => {
  const [anchorEl, setAnchorEl] = React.useState(null);
  const handleClick = (event) => setAnchorEl(event.currentTarget);
  const handleClose = () => setAnchorEl(null);
  const translate = useTranslate();

  return (
    <>
      <Button color="primary" variant="text" aria-controls="create-objects" aria-haspopup="true" onClick={handleClick}>
        {translate(`specialLabels.createNewSequenceItem`)}
      </Button>
      <Menu
        id="simple-menu"
        anchorEl={anchorEl}
        keepMounted
        open={Boolean(anchorEl)}
        onClose={handleClose}
      >
        <MenuItem onClick={() => addHylem("add")}>{translate(`specialLabels.Hylem`)}</MenuItem>
        <MenuItem onClick={() => addGap("add")}>{translate(`specialLabels.Gap`)}</MenuItem>
        <MenuItem onClick={() => addHeadline("add")}>{translate(`specialLabels.Headline`)}</MenuItem>
      </Menu>
    </>
  )
}

const Settings = styled.div`
  display: flex;
  justify-content: space-between;
`;

export default HylemList;
