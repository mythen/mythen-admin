import {ListGuesser} from '@api-platform/admin';
import React from 'react';
import {
  Datagrid,
  Filter,
  FunctionField,
  ReferenceArrayField,
  ReferenceField,
  SimpleShowLayout,
  Show,
  TextField,
  TextInput,
} from 'react-admin';
import {Checkbox} from '@material-ui/core';
import { isAdmin } from '../../services/token.service';
import { ShowTitle } from '../util/Tools';
import { useTranslate } from 'react-admin';

const HylemFilter = (props) => (
  <Filter {...props}>
    <TextInput label={'specialLabels.filters.hySpo'} source="hySpo" />
    <TextInput label={'specialLabels.filters.textReference'} source="textReference" />
    <TextInput label={'specialLabels.filters.originalText'} source="originalText" />
  </Filter>
)

export const HylemList = (props) => {
  return (
    <ListGuesser {...props} perPage={25} sort={{ field: 'id', order: 'DESC' }} bulkActionButtons={isAdmin()} filters={<HylemFilter />} >
      <FunctionField
        label="ID"
        sortBy="id"
        render={record => (record.id).replace('/api/hylems/', '')}
      />
      <TextField source="hySpo" label="SPO" />
      <ReferenceArrayField
        className={'no-table-style'}
        label={'specialLabels.subjectsPlusDeterminations'}
        source="subjectDeterminations"
        reference="subject_determinations"
        sortable={false}
      >
        <Datagrid>
          <FunctionField
            render={subjectDeterminations => subjectDeterminations.subjectDetermination ? (`${subjectDeterminations.subject.subject} → ${subjectDeterminations.subjectDetermination}`) : (`${subjectDeterminations.subject.subject}`)}
          />
        </Datagrid>
      </ReferenceArrayField>
      <ReferenceField
        className={'no-table-style'}
        label={'specialLabels.predicatesPlusDeterminations'}
        source="predicateDetermination"
        reference="predicate_determinations"
        sortable={false}
        link={false}
      >
        <FunctionField
          render={predicateDetermination => predicateDetermination.predicateDetermination ? (`${predicateDetermination.predicate.predicate} → ${predicateDetermination.predicateDetermination}`) : (`${predicateDetermination.predicate.predicate}`)}
        />
      </ReferenceField>
      <ReferenceArrayField
        className={'no-table-style'}
        label={'specialLabels.hylemObjectsPlusDeterminations'}
        source="hylemObjectDeterminations"
        reference="hylem_object_determinations"
        sortable={false}
      >
        <Datagrid>
          <FunctionField
            render={hylemObjectDeterminations => hylemObjectDeterminations.hylemObjectDetermination ? (`${hylemObjectDeterminations.hylemObject.hylemObject} → ${hylemObjectDeterminations.hylemObjectDetermination}`) : (`${hylemObjectDeterminations.hylemObject.hylemObject}`)}
          />
        </Datagrid>
      </ReferenceArrayField>
      <TextField source="textReference" label={'specialLabels.textReference'} />
      <TextField source="originalText" label={'specialLabels.originalText'} />
    </ListGuesser>
  );
}

export const HylemShow = (props) => {
  const translate = useTranslate();
  return (
    <Show {...props} title={<ShowTitle />} className={'hylem-simple-show-view'}>
      <SimpleShowLayout>
        {/* Subject Determination */}
        <ReferenceArrayField
          className={'no-table-style'}
          label={'specialLabels.subjectsPlusDeterminations'}
          source="subjectDeterminations"
          reference="subject_determinations"
        >
          <Datagrid>
            <FunctionField
              render={subjectDeterminations => subjectDeterminations.subjectDetermination ? (`${subjectDeterminations.subject.subject} → ${subjectDeterminations.subjectDetermination}`) : (`${subjectDeterminations.subject.subject}`)}
            />
          </Datagrid>
        </ReferenceArrayField>
        {/* Predicate Determination */}
        <ReferenceField
          label={'specialLabels.predicatesPlusDeterminations'}
          source="predicateDetermination"
          reference="predicate_determinations"
          link={false}
        >
          <FunctionField
            render={predicateDetermination => predicateDetermination.predicateDetermination ? (`${predicateDetermination.predicate.predicate} → ${predicateDetermination.predicateDetermination}`) : (`${predicateDetermination.predicate.predicate}`)}
          />
        </ReferenceField>
        {/* Hylem Object Determination */}
        <ReferenceArrayField
          className={'no-table-style'}
          label={'specialLabels.hylemObjectsPlusDeterminations'}
          source="hylemObjectDeterminations"
          reference="hylem_object_determinations"
        >
          <Datagrid>
            <FunctionField
              render={hylemObjectDeterminations => hylemObjectDeterminations.hylemObjectDetermination ? (`${hylemObjectDeterminations.hylemObject.hylemObject} → ${hylemObjectDeterminations.hylemObjectDetermination}`) : (`${hylemObjectDeterminations.hylemObject.hylemObject}`)}
            />
          </Datagrid>
        </ReferenceArrayField>
        <TextField source="hySpo" label="Hylem SPO" />
        <TextField source="textReference" label={'specialLabels.textReference'} />
        <TextField source="originalText" label={'specialLabels.originalText'} />
        <TextField source="translation" label={'specialLabels.translation'} />
        <label className={'custom-label-shrink'}>
          {translate('specialLabels.furtherInformation')}
        </label>
        <ShowCheckbox checkedKey={"implicit"} label={translate('specialLabels.implicit')} />
        <ShowCheckbox checkedKey={"indirect"} label={translate('specialLabels.indirect')} />
      </SimpleShowLayout>
    </Show>
  );
}

const ShowCheckbox = ({ record, checkedKey, label }) => (
  <div className={'custom-checkbox-hylems'}>
    <Checkbox
      checked={record[checkedKey]}
      readOnly
      disabled
    />
    {label}
  </div>
)
