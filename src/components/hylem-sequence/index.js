import {
  Chip,
  CircularProgress
} from '@material-ui/core';
import {
  ChipField,
  Create,
  EditButton,
  FunctionField,
  ReferenceArrayField,
  ReferenceArrayInput,
  SelectArrayInput,
  Show,
  SimpleForm,
  SimpleShowLayout,
  SingleFieldList,
  Tab,
  TabbedShowLayout,
  TextField,
  TextInput,
  useTranslate,
  required
} from 'react-admin';
import EditGuesser from '@api-platform/admin/lib/EditGuesser';
import { EditTitle, ShowTitle } from '../util/Tools';
import HylemList from './List';
import InputGuesser from '@api-platform/admin/lib/InputGuesser';
import { isAdmin } from '../../services/token.service';
import { ListGuesser, FieldGuesser } from '@api-platform/admin';
import React from 'react';
import styled from 'styled-components'
import { styles } from '../../style';
import { useGetNestedApi } from '../../hooks/useGetNestedApi';
import { useSort } from '../../hooks/useSort';

//  Create - Hylem Sequence
export const HylemSequenceCreate = (props) => (
  <Create {...props}>
    <SimpleForm>
      <TextInput source="name" validate={[required()]} />
      <ReferenceArrayInput source="sources" reference="sources">
        <SelectArrayInput optionText="title" />
      </ReferenceArrayInput>
      <ReferenceArrayInput source="myths" reference="myths">
        <SelectArrayInput optionText="title" />
      </ReferenceArrayInput>
      <InputGuesser source={'editor'} />
    </SimpleForm>
  </Create>
);

export const HylemSequenceEdit = (props) => {
  const translate = useTranslate();
  return (
    <Show {...props} title={<EditTitle/>}>
      <SimpleShowLayout>
        <TabbedShowLayout className="fullwidth-tabs">
          <Tab label="specialLabels.context">
            <EditGuesser {...props} title={translate(`specialLabels.emptyTitle`)}>
              <InputGuesser source={'name'} validate={[required()]} />
              <ReferenceArrayInput source="sources" reference="sources">
                <SelectArrayInput optionText="title"/>
              </ReferenceArrayInput>
              <ReferenceArrayInput source="myths" reference="myths">
                <SelectArrayInput optionText="title"/>
              </ReferenceArrayInput>
              <InputGuesser source={'editor'} />
            </EditGuesser>
          </Tab>
          <Tab label="specialLabels.sequenceItems" path="items">
            <HylemList {...props} />
          </Tab>
        </TabbedShowLayout>
      </SimpleShowLayout>
    </Show>
  );
};

// List - Hylem Sequence
const HylemsLength = ({ record }) => <div>{record?.itemsTime?.length}</div>

export const HylemSequenceList = (props) => (
  <ListGuesser {...props} perPage={25} bulkActionButtons={isAdmin()}>
    <FunctionField
      label="ID"
      sortBy="id"
      render={record => (record.id).replace('/api/hylem_sequences/', '')}
    />
    <HylemsLength label="specialLabels.hylemsLength" />
    <FieldGuesser source="name" />
    <FieldGuesser source="editor" />
  </ListGuesser>
);

// Show - Hylem Sequence
export const HylemSequenceShow = (props) => {
  const translate = useTranslate();
  return (
    <Show {...props} title={<ShowTitle />}>
      <SimpleShowLayout>
        <div style={styles.title}>{translate(`specialLabels.context`)}</div>
        <TextField source="name" />

        <ReferenceArrayField source="sources" reference="sources">
          <SingleFieldList>
            <ChipField source="title" />
          </SingleFieldList>
        </ReferenceArrayField>

        <ReferenceArrayField source="myths" reference="myths">
          <SingleFieldList>
            <ChipField source="title" />
          </SingleFieldList>
        </ReferenceArrayField>

        <TextField source="editor" />

        <br/>
        <div style={styles.title}>{translate(`specialLabels.hylemsGapsHeadlines`)}</div>
        <DisplayOrders />
        {isAdmin() && <EditButton style={{ marginTop: 40 }} />}
      </SimpleShowLayout>
    </Show>
  );
};

// Hylems
const data = {
  Hylem: {
    color: 'default',
    display: 'hySpo',
  },
  Gap: {
    color: 'primary',
    display: 'gapRemarks',
  },
  Headline: {
    color: 'secondary',
    display: 'headlineRemarks',
  }
}

const DisplayOrders = ({ record }) => {
  const { finalData: list, loading } = useGetNestedApi({ array: record.itemsTime, sorting: true, isOnce: true, lazyLoading: true })
  const { sortedArr, SortOrder } = useSort(list);

  return !loading ? (
    <Wrapper>
      {/* Sort Order */}
      <SortOrder />
      {/* Show hylem SPO */}
      <div>
        {sortedArr.map((item, i) => {
          const { color, display } = data[item['@type']]
          return (
            <Chip color={color} label={item[display]} style={{ margin: 5 }} key={`h${i}`} />
          )
        })}
      </div>
    </Wrapper>
  ) : <CircularProgress />
}

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  margin-top: 20px;
`;
