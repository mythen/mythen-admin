import { Button, IconButton, LinearProgress, Typography } from '@material-ui/core';
import CustomModal from '../../UI/CustomModal';
import { CheckBox, CustomInput, CustomRadio } from '../../UI/Form';
import { Delete } from '@material-ui/icons';
import {
  addHylemTime,
  determinationsMap,
  modifyDeterminations,
  seperateData
} from '../../../services/hylem.service';
import React, { useRef, useState, } from 'react';
import styled from 'styled-components';
import { useFetchList } from '../../../hooks/useFetchList';
import { useFieldArray, useForm } from 'react-hook-form';
import { useTranslate } from 'react-admin';

export default function AddHylemTime({ hylemTime, modal, setModal, selHylem, sequenceLength }) {
  const submitInput = useRef(null);
  const [error, setError] = useState(null);
  const [loading, setLoading] = useState(false);
  const { register, control, setValue, handleSubmit } = useForm(); // For Hylem & Determinations
  const listTypes = useFetchList('types', 'type');
  const translate = useTranslate();
  const sortValue = sequenceLength;

  // On Submit Form - create a new hylem
  const handelSubmitForm = handleSubmit(async (data) => {
    const { hylemData, deterData } = seperateData(data);
    if (deterData['/subject_determinations'] && deterData['/predicate_determinations']) {
      setLoading(true);
      await modifyDeterminations(deterData, (m) => {
        // on failed to add determination
        setError(m);
        setLoading(false);
      }, async (deters) => {
        // On Success
        await addHylemTime(hylemTime, hylemData, deters, selHylem, sortValue, setError);
        setLoading(false);
      });
    } else {
      alert('Cannot save Hylem. Minimum requirements: 1 Subject and 1 Predicate')
    }
  });

  return (
    <CustomModal
      className="hylem-dialog"
      close={() => setModal('add')}
      error={error}
      loading={loading}
      okHandle={() => submitInput.current.click()}
      open={modal}
      title={translate('specialLabels.addHylem')}
    >
      <form onSubmit={handelSubmitForm}>
        <div className="hylem-form">
          {/* Hylem SPO */}
          <Typography className="hylemTitleMargin" variant="h5" style={{ marginTop: 0 }}>SPO</Typography>
          <AllDeterminations setValue={setValue} register={register} control={control} />
          <CustomInput name="hySpo" refer={register} placeholder="SPO" />
          {/* Text Reference */}
          <Typography variant="h5">{translate(`specialLabels.textReference`)}</Typography>
          <div className="formedit">
            <CustomInput name="textReference" refer={register} label={translate(`specialLabels.textReference`)} placeholder={translate(`specialLabels.yourTextReference`)} />
            <CustomInput name="originalText" refer={register} label={translate(`specialLabels.originalText`)} placeholder={translate(`specialLabels.yourOriginalText`)} />
            <CustomInput name="translation" refer={register} label={translate(`specialLabels.translation`)} placeholder={translate(`specialLabels.yourTranslation`)} />
          </div>
          <CheckBox name="implicit" label={translate(`specialLabels.implicit`)} control={control} defaultValue={false} />
          <CheckBox name="indirect" label={translate(`specialLabels.indirect`)} control={control} defaultValue={false} />
          {/* Further Information */}
          {listTypes && <CustomRadio
            name={'type'}
            className="select-search"
            options={listTypes}
            label={translate(`resources.types.fields.type`)}
            refer={register()}
            defaultValue={'/api/types/1'}
            empty
          />}
        </div>
        <input ref={submitInput} type="submit" style={{ display: 'none' }} />
      </form>
    </CustomModal>
  );
}

// All Determinations Subject, Hylem Object, Predicate, ..
const AllDeterminations = ({ ...restDet }) => {
  return (
    <div>
      {determinationsMap.map(({ ...rest }) => (
        <DeterminationList
          {...restDet}
          {...rest}
          key={Math.random().toString()}
        />
      ))}
    </div>
  );
};

function DeterminationList({ single, apiDeter, listName, control, keyDet, setValue, ...rest }) {
  const [addSingle, setAddSingle] = useState(single);
  const list = useFetchList(listName, keyDet);
  const translate = useTranslate();
  const { fields, append } = useFieldArray({
    control,
    name: apiDeter,
  });
  const remove = (index) => {
    setValue(apiDeter, fields.filter((_, i) => index !== i));
    single && setAddSingle(true);
  };

  return (
    <WrapperDetList>
      {/* list all determinations */}
      {
        fields.map((field, i) => <Determination
          options={list}
          key={field.id}
          api={apiDeter}
          index={i}
          keyDet={keyDet}
          remove={remove}
          field={field}
          {...rest} />)
      }
      {/* Add Determination */}
      {(!single || addSingle) && <Button color={'secondary'} onClick={() => {
        append({ name: apiDeter });
        addSingle && setAddSingle(false);
      }}>

        {translate(`specialLabels.add.${keyDet}`)}
      </Button>}
    </WrapperDetList>
  );
}

function Determination({ api, field, index, register, keyDet, detText, options, label, remove }) {
  const translate = useTranslate();
  const requireSubjectPredicateFlag = (keyDet === 'subject' || keyDet === 'predicate') ? ' *' : '';
  const requireSubjectPredicate = (keyDet === 'subject' || keyDet === 'predicate');
  return (
    options != null ?
      (<div className="determination">
        <CustomRadio
          className="select-search"
          empty
          label={translate(`specialLabels.${keyDet}`)+requireSubjectPredicateFlag}
          name={`${api}[${index}].${keyDet}`}
          options={options}
          refer={register()}
          defaultValue={field[detText]}
          required={requireSubjectPredicate}
        />
        <CustomInput
          label={translate(`specialLabels.${label}`)}
          name={`${api}[${index}].${detText}`}
          refer={register()}
          defaultValue={field[detText]}
          placeholder={translate(`specialLabels.optionalDetarmination`)}
        />
        <IconButton
          className="delbtn"
          color={'primary'}
          variant={'contained'}
          title={translate(`specialLabels.remove`)}
          onClick={() => remove(index)}
        >
          <Delete fontSize={'small'} />
        </IconButton>
      </div>)
      : <LinearProgress color="secondary" style={{ width: '200px', margin: '40px auto' }} />
  );
}

const WrapperDetList = styled.div`
  border-bottom: 1px dashed rgb(199 199 199)
`;
