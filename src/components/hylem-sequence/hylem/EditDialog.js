import { Button, IconButton, LinearProgress, Typography } from '@material-ui/core';
import { CheckBox, CustomInput, CustomRadio } from '../../UI/Form';
import CustomModal from '../../UI/CustomModal';
import { Delete } from '@material-ui/icons';
import {
  determinationsMap,
  updateHylemTime,
  modifyDeterminations,
  seperateData
} from '../../../services/hylem.service';
import React, { useRef, useState } from 'react';
import { removeApiTxt } from '../../../server/api';
import styled from 'styled-components';
import { useFetchList } from '../../../hooks/useFetchList';
import { useFieldArray, useForm } from 'react-hook-form';
import useSWR from 'swr';
import { useGetDefDeter } from '../../../hooks/useGetDefDeters';
import { useTranslate } from 'react-admin';

export default function HylemTime({ hylemId, modal, setModal }) {
  const { data } = useSWR(removeApiTxt(hylemId));
  const [error, setError] = useState(null);
  const [loading, setLoading] = useState(false);
  const submitInput = useRef(null);
  const translate = useTranslate();
  const titleString = translate(`dynamicTitles.edit`) + " " + translate(`dynamicTitles.titleHylem`) +" #" + hylemId.toString().substring((hylemId).lastIndexOf("/") + 1);

  return (
    <CustomModal
      className="hylem-dialog"
      error={error}
      loading={loading}
      okHandle={() => submitInput.current.click()}
      title={titleString}
      close={() => setModal('edit')}
      open={modal}
    >
      {
        data ?
          <HylemBody hylemId={hylemId} hylem={data} submitRef={submitInput} setLoading={setLoading} setError={setError} />
          : <LinearProgress color="secondary" style={{ width: '200px', margin: '40px auto' }} />
      }
    </CustomModal>
  );
}

const HylemBody = ({ hylemId, hylem, submitRef, setLoading, setError }) => {
  const { register, handleSubmit, setValue, control } = useForm({ defaultValues: { ...hylem } }); // for hylem
  // On Submit Form - update hylem
  const handelSubmitForm = handleSubmit(async (data) => {
  const { hylemData, deterData } = seperateData(data);
    setLoading(true);
    await modifyDeterminations(deterData, (m) => {
      // on failed to add determination
      setError(m);
      setLoading(false);
    }, async (deters, deletedDeters) => {
      // On Success
      await updateHylemTime(hylemId, hylem, hylemData, deters, setError, deletedDeters);
      setLoading(false);
    });
  });
  const listTypes = useFetchList('types', 'type');

  const translate = useTranslate();
  return (
    <form onSubmit={handelSubmitForm}>
      <div className="hylem-form">
        {/* Hylem SPO */}
        <Typography className="hylemTitleMargin" variant="h5" style={{ marginTop: 0 }}>SPO</Typography>
        {<AllDeterminations hylem={hylem} register={register} setValue={setValue} control={control} />}
        <CustomInput name="hySpo" refer={register} placeholder="SPO" />
        {/* Text Reference */}
        <Typography variant="h5">{translate(`specialLabels.textReference`)}</Typography>
        <div className="formedit">
          <CustomInput name="textReference" refer={register} label={translate(`specialLabels.textReference`)} placeholder={translate(`specialLabels.yourTextReference`)} />
          <CustomInput name="originalText" refer={register} label={translate(`specialLabels.originalText`)} placeholder={translate(`specialLabels.yourOriginalText`)} />
          <CustomInput name="translation" refer={register} label={translate(`specialLabels.translation`)} placeholder={translate(`specialLabels.yourTranslation`)} />
        </div>
        <CheckBox name="implicit" label={translate(`specialLabels.implicit`)} control={control} defaultValue={false} />
        <CheckBox name="indirect" label={translate(`specialLabels.indirect`)} control={control} defaultValue={false} />
        {/* Further Information */}
        {listTypes && <CustomRadio
          name={'type'}
          className="select-search"
          options={listTypes}
          label={translate(`resources.types.fields.type`)}
          refer={register()}
          defaultValue={'/api/types/1'}
          empty
        />}
      </div>
      <input ref={submitRef} type="submit" style={{ display: 'none' }} />
    </form>
  );
};

// All Determinations Subject, Hylem Object, Predicate, ..
const AllDeterminations = ({ ...restDet }) => {
  return (
    <div>
      {determinationsMap.map(({ ...rest }) => (
        <DeterminationList
          {...restDet}
          {...rest}
          key={Math.random().toString()}
        />
      ))}
    </div>
  );
};

function DeterminationList({ hylem, single, apiDeter, setValue, listName, control, detList, keyDet, ...rest }) {
  const [addSingle, setAddSingle] = useState(single);
  const { defDeters } = useGetDefDeter({ hylem, keyDet, apiDeter, detList, ...rest }); // get default deters..
  const list = useFetchList(listName, keyDet);
  const translate = useTranslate();
  const { fields, append } = useFieldArray({
    control,
    name: apiDeter,
  });
  const singleOldItem = single && defDeters.length === 0;
  const remove = (index) => {
    setValue(apiDeter, fields.filter((_, i) => index !== i));
    single && setAddSingle(true);
  };

  const removeDeter = (api) => {
    setValue(api, { ...defDeters.find((d) => d.api === api), deleted: true });
  };

  return (
    <WrapperDetList>
      {/* list all old/default determinations */}
      {
        defDeters.map((det, i) => <Determination
          api={det.api}
          data={det}
          index={i}
          key={`${i}ixa`}
          keyDet={keyDet}
          options={list}
          remove={removeDeter}
          old={true}
          {...rest} />)
      }
      {/* list all new determinations */}
      {
        fields.map((_, i) => <Determination
            options={list}
            key={_.id}
            api={apiDeter}
            index={i}
            keyDet={keyDet}
            remove={remove}
            {...rest} />)
      }
      {/* Add Determination */}
      {(!single || addSingle) && singleOldItem !== false &&
        <Button color={'secondary'} onClick={() => {
          append({ name: apiDeter });
          addSingle && setAddSingle(false);
        }}>

          {translate(`specialLabels.add.${keyDet}`)}
        </Button>
      }
    </WrapperDetList>
  );
}

function Determination({ data = {}, api, old = false, remove, index, register, keyDet, detText, options, label }) {
  const finalName = data.api ? api : `${api}[${index}]`;
  const handleRemove = old ? () => remove(api) : () => remove(index);
  const translate = useTranslate();
  const requireSubjectPredicateFlag = (keyDet === 'subject' || keyDet === 'predicate') ? ' *' : '';
  const requireSubjectPredicate = (keyDet === 'subject' || keyDet === 'predicate');

  return (
    options != null ?
      (
        <div className="determination">
          <CustomRadio
            className="select-search"
            defaultValue={data[keyDet] || ''}
            empty
            label={translate(`specialLabels.${keyDet}`)+requireSubjectPredicateFlag}
            name={`${finalName}.${keyDet}`}
            options={options}
            refer={register()}
            required={requireSubjectPredicate}
          />
          <CustomInput
            name={`${finalName}.deleted`}
            defaultValue={data.deleted || false}
            refer={register}
            displayNone
          />
          <CustomInput
            defaultValue={data[detText] || ''}
            label={translate(`specialLabels.${label}`)}
            name={`${finalName}.${detText}`}
            refer={register}
            placeholder={translate(`specialLabels.optionalDetarmination`)}
          />
          <DeleteButton handleRemove={handleRemove} />
        </div>
      )
      : <LinearProgress color="secondary" style={{ width: '200px', margin: '40px auto' }} />
  );
}

const DeleteButton = ({ handleRemove }) => {
  const [del, setDel] = useState(false);
  const translate = useTranslate();

  return (
    <>
      {del && <Deleted>D</Deleted>}
      <IconButton
        className="delbtn"
        color={'primary'}
        variant={'contained'}
        title={translate(`specialLabels.remove`)}
        onClick={() => {
            setDel(true)
            handleRemove()
        }}
      >
        <Delete fontSize={'small'} />
      </IconButton>
    </>
  )
}
const Deleted = styled.div`
  color: red;
  font-weight: bold;
`;

const WrapperDetList = styled.div`
  border-bottom: 1px dashed rgb(199 199 199)
`;
