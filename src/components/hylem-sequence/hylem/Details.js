import { Checkbox } from '@material-ui/core';
import CustomTabs from '../../UI/Tabs';
import {
  ReferenceArrayField,
  ReferenceField,
  Show,
  SimpleShowLayout,
  SingleFieldList,
  TextField,
  useTranslate
} from 'react-admin';
import React from 'react';
import styled from 'styled-components';

const detailsTab = ['Hylem SPO', 'Text reference', 'Further information'];

// Display Subject
const DisplaySubject = ({ record }) => (
  <Label>
    {record?.subject.subject}
    <span>{record?.subjectDetermination}</span>
  </Label>
)

// Display Predicate
const DisplayPredicate = ({ record }) => (
  <Label>
    {record?.predicate.predicate}
    <span>{record?.predicateDetermination || "-"}</span>
  </Label>
)

const DisplayObject = ({ record }) => (
  <Label>
    {record?.hylemObject.hylemObject}
    <span>{record?.hylemObjectDetermination}</span>
  </Label>
)

// Hylem SPO
// --------------------
const HylemSPO = (props) => {
  const translate = useTranslate();
  return (
    <Show hasEdit={false} {...props} title={translate(`specialLabels.emptyTitle`)} >
      <SimpleShowLayout>
        {/* Display Subject */}
        <ReferenceArrayField
          className="columnDir"
          label={'resources.subjects.name'}
          source="subjectDeterminations"
          reference="hylems"
          link={false}
        >
          <SingleFieldList linkType={false}>
            <DisplaySubject />
          </SingleFieldList>
        </ReferenceArrayField>
        {/* Predicate Determination */}
        <ReferenceField
          label={'resources.predicates.fields.predicate'}
          source="predicateDetermination"
          reference="hylems"
          link={false}
        >
          <DisplayPredicate />
        </ReferenceField>
        {/* Hylem Objects Determinations */}
        <ReferenceArrayField
          className="columnDir"
          label={'resources.hylem_objects.name'}
          source="hylemObjectDeterminations"
          reference="hylem_object_determinations"
          link={false}
        >
          <SingleFieldList linkType={false}>
            <DisplayObject />
          </SingleFieldList>
        </ReferenceArrayField>
        {/* Hylem SPO */}
        <TextField source="hySpo" label="Hylem SPO" />
      </SimpleShowLayout>
    </Show>
  )
}

// Text reference
// --------------------
const ShowCheckbox = ({ record, checkedKey, label }) => (
  <div>
    <Checkbox
      checked={record[checkedKey]}
      readOnly
      disabled
    />
    {label}
  </div>
)

const TxtReference = (props) => {
  const translate = useTranslate();
  return (
    <Show hasEdit={false} {...props} title={translate(`specialLabels.emptyTitle`)} >
      <SimpleShowLayout>
        <TextField source="textReference" label={'specialLabels.textReference'} />
        <TextField source="originalText" label={'specialLabels.originalText'} />
        <TextField source="translation" label={'specialLabels.translation'} />
        <DividerLine />
        <ShowCheckbox checkedKey={"implicit"} label={translate(`specialLabels.implicit`)} />
        <ShowCheckbox checkedKey={"indirect"} label={translate(`specialLabels.indirect`)} />
      </SimpleShowLayout>
    </Show>
  );
}

// Further information
// --------------------
const FurInfo = (props) => {
  const translate = useTranslate();
  return (
    <Show hasEdit={false} {...props} title={translate(`specialLabels.emptyTitle`)} >
      <SimpleShowLayout>
        {/* Hylem types */}
        <ReferenceField source="type" reference="types" link={false} label={translate(`resources.types.fields.type`)} >
          <TextField source="type" />
        </ReferenceField>
      </SimpleShowLayout>
    </Show>
  );
}

export default (props) => {
  const translate = useTranslate();
  return (
      <Wrapper>
        <CustomTabs items={detailsTab} title={translate(`specialLabels.emptyTitle`)} >
          <HylemSPO {...props} />
          <TxtReference {...props} />
          <FurInfo {...props} />
        </CustomTabs>
      </Wrapper>
  );
}

const Wrapper = styled.div`
  clear: both;
  background: white;
  border: 2px solid #f6f6f6;
  .MuiPaper-elevation1 {
    box-shadow: none !important;
  }
  .MuiBox-root {
    padding: 0 !important;
  }
  .MuiCardContent-root {
    padding-top: 0 !important;
    margin-top: -10px !important;
  }
`;

const Label = styled.p`
  span {
    font-weight: bold;
    margin-left: 20px;
  }
`;

const DividerLine = styled.p`
  width: 100%;
  height: 1px;
  background-color: #ccc
`;
