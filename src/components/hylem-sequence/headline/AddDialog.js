import { addHeadline } from '../../../services/headline.service';
import { CustomInput } from '../../UI/Form';
import CustomModal from '../../UI/CustomModal';
import React, { useRef, useState, } from 'react';
import { Typography } from '@material-ui/core';
import { useForm } from 'react-hook-form';
import { useTranslate } from 'react-admin';

export default function AddHeadline({ hylemTime, modal, setModal, sequenceLength }) {
  const submitInput = useRef(null);
  const [error, setError] = useState(null);
  const [loading, setLoading] = useState(false);
  const { register, handleSubmit } = useForm(); // For Hylem & Determinations
  const translate = useTranslate();
  const sortValue = sequenceLength;

  // On Submit Form - create a new Headline
  const handelSubmitForm = handleSubmit(async (data) => {
    setLoading(true);
    await addHeadline(hylemTime, data, sortValue, setError);
    setLoading(false);
  });

  return (
    <CustomModal
      className="hylem-dialog"
      title={translate('specialLabels.addHeadline')}
      open={modal}
      close={() => setModal('add')}
      loading={loading}
      error={error}
      okHandle={() => submitInput.current.click()}
    >
      <form onSubmit={handelSubmitForm}>
        <div className="hylem-form">
          {/* Text Reference */}
          <Typography variant="h5">Details</Typography>
          <div className="formedit">
            <CustomInput
              name="headlineRemarks"
              label={translate('specialLabels.headlineRemarks')+' *'}
              placeholder={translate('specialLabels.yourHeadlineRemarks')}
              refer={register}
              required={true}
            />
          </div>
        </div>
        <input ref={submitInput} type="submit" style={{ display: 'none' }} />
      </form>
    </CustomModal>
  );
};
