import { CustomInput } from '../../UI/Form';
import CustomModal from '../../UI/CustomModal';
import React, { useRef, useState, } from 'react';
import { LinearProgress, Typography } from '@material-ui/core';
import { useForm } from 'react-hook-form';
import useSWR from 'swr';
import { removeApiTxt } from '../../../server/api';
import { editHeadline } from '../../../services/headline.service';
import { useTranslate } from 'react-admin';

export default function HeadlineTime({ headlineId, modal, setModal }) {
  const submitInput = useRef(null);
  const [error, setError] = useState(null);
  const [loading, setLoading] = useState(false)
  const { data } = useSWR(removeApiTxt(headlineId))
  const translate = useTranslate();
  const titleString = translate(`dynamicTitles.edit`) + " " + translate(`dynamicTitles.titleHeadline`) +" #" + headlineId.toString().substring((headlineId).lastIndexOf("/") + 1);

  return (
    <CustomModal
      className="hylem-dialog"
      title={titleString}
      loading={loading}
      error={error}
      okHandle={() => submitInput.current.click()}
      close={() => setModal('edit')}
      open={modal}
    >
      {
        data ?
          <HeadlineBody
            headlineId={headlineId}
            headline={data}
            submitRef={submitInput}
            setLoading={setLoading}
            setError={setError}
          />
          : <LinearProgress color="secondary" style={{ width: '200px', margin: '40px auto' }} />
      }

    </CustomModal>
  );
};

const HeadlineBody = ({ headlineId, headline, submitRef, setLoading, setError }) => {
  const { register, handleSubmit } = useForm({ defaultValues: { ...headline } }); // for hylem
  // On Submit Form - update headline
  const handelSubmitForm = handleSubmit(async (data) => {
    setLoading(true);
    editHeadline(headlineId, data, setError);
    setLoading(false);
  });
  const translate = useTranslate();

  return (
    <form onSubmit={handelSubmitForm}>
      <div className="hylem-form">
        {/* Text Reference */}
        <Typography variant="h5">Details</Typography>
        <div className="formedit">
          <CustomInput
            name="headlineRemarks"
            refer={register}
            label={translate('specialLabels.headlineRemarks')+' *'}
            placeholder={translate('specialLabels.yourHeadlineRemarks')}
            required={true}
          />
        </div>
      </div>
      <input ref={submitRef} type="submit" style={{ display: 'none' }} />
    </form>
  );
};
