import axios from 'axios';

const apiLink = process.env.REACT_APP_HYDRA_ADMIN_ENDPOINT;

const api = axios.create({
  baseURL: apiLink,
  headers: {
    Authorization: `Bearer ${localStorage.getItem('token')}`,
  },
});

api.interceptors.response.use(
  (res) => {
    const { data } = res;
    return Promise.resolve({ data, res });
  },

  (err) => {
    const status = err.response.status;

    if (status === 401) {
      if (localStorage.getItem('token')) {
        localStorage.removeItem('token');
        window.location.reload();
      }
    };

    return Promise.reject(err);
  }
);

export const removeApiTxt = (str) => typeof str === 'string' && str.replace('/api', '');

// Fetcher for SWR
export const fetcherSWR = async (endpoint) => {
  const res = await api.get(endpoint);

  return res.data;
};

export default api;
