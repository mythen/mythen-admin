import { resolveBrowserLocale } from 'react-admin';
import polyglotI18nProvider from 'ra-i18n-polyglot';

// interface translations
import germanMessages from 'ra-language-german';
import englishMessages from 'ra-language-english';
import germanMessagesAdditions from './raAdditionalMessages';

// domain translations
import domainMessages from './messages';

const messages = {
  de: { ...germanMessages, ...domainMessages.de, ...germanMessagesAdditions },
  en: { ...englishMessages, ...domainMessages.en },
};

const i18nProvider = polyglotI18nProvider(
  locale => messages[locale] ? messages[locale] : messages.en,
  resolveBrowserLocale()
);

export default i18nProvider;