# Mythen-Admin

## Installation

As prerequisites [yarn](https://yarnpkg.com/lang/en/) and [nvm](https://github.com/nvm-sh/nvm) need to be present in the system.

* `nvm use` switches to the desired NodeJS version determined in `.nvmrc`
* `yarn install` installs all dependencies

Add a file named `.env.local` in the root directory of this project and change the values of
`REACT_APP_ENDPOINT` and `REACT_APP_HYRA_ADMIN_ENDPOINT` to the ones that match your environment, e.g.:
* `REACT_APP_ENDPOINT=http://localhost:8866`
* `REACT_APP_HYDRA_ADMIN_ENDPOINT=http://localhost:8866/api`

### JWT authentication

The ReactAdmin needs to authenticate with _username_ and _password_ to use the API. Therefore we use JSON Web Tokens (JWT, [https://jwt.io](https://jwt.io)).
For local development you have to generate a JWT SSH keypair first and provide the keys as PEM files in `mythen-app` (see **GitLab "/mythen/mythen-app":** [README.md#jwt-authentication](https://gitlab.gwdg.de/mythen/mythen-app/-/blob/4404af730035ff5d7a0b2258611f4e1f1c6827dd/README.md#jwt-authentication)).

If you just want to check the data model or try out GET/POST/PUT/DELETE methods directly via your ApiPlatform in a browser at `%REACT_APP_HYDRA_ADMIN_ENDPOINT%/docs` you need to authenticate manually - here's a little **how-to**:
1. Open a command line interface
2. Enter the following command using your desired/configured authentication endpoint:
   ```shell
   curl --header "Content-Type: application/json" \
   --request POST \
   --data '{"username":"admin","password":"admin"}' \
   http://localhost:8866/v1/authentication_token
   ```
3. Copy the value of `token` from the JSON-encoded response to your clipboard:
   ```json
   {"token":"eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJpYXQiOjE2M[...]jyH7my_1AqI","refresh_token":"9ab123f1[...]5525ee0f"}
   ```
4. Open the URL of the respective Hydra endpoint (compare step 2), e.g. `http://localhost:8866/api/docs`
5. In the upper right click the green button labelled "Authorize"
6. In the field labelled "Value" insert the word "Bearer " (plus space) followed by the token from step 3:
   ```text
   Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJpYXQiOjE2M[...]jyH7my_1AqI
   ```
7. Click "Authorize" button and enjoy trying out GET/POST/PUT/DELETE (recommendation: DELETE only in local environments) for your entities

## Available Scripts

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

### `yarn test`

Launches the test runner in the interactive watch mode.<br>
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `yarn run build`

Builds the app for production to the `build` folder.<br>
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br>
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `yarn run eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (Webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).

### Code Splitting

This section has moved here: https://facebook.github.io/create-react-app/docs/code-splitting

### Analyzing the Bundle Size

This section has moved here: https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size

### Making a Progressive Web App

This section has moved here: https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app

### Advanced Configuration

This section has moved here: https://facebook.github.io/create-react-app/docs/advanced-configuration

### Deployment

This section has moved here: https://facebook.github.io/create-react-app/docs/deployment

### `yarn run build` fails to minify

This section has moved here: https://facebook.github.io/create-react-app/docs/troubleshooting#yarn-run-build-fails-to-minify
